#ocamlbuild aurait pu être utilisé. 

LIB_DIR=lib
SRC_DIR=src
OCAMLBUILD=ocamlc
OCAMLLEX=ocamllex
OCAMLPARSER=ocamlyacc

exec: fsm.cmo regex.cmo nfa.cmo dfa.cmo dfa_min.cmo graphics_utils.cmo gnfa.cmo automaton_drawing.cmo lexer.ml parser.ml parser.mli parser.cmi lexer.cmo parser.cmo main.cmo

	$(OCAMLBUILD) -o sortie -I +lablgtk2 graphics.cma unix.cma $(LIB_DIR)/fsm.cmo $(LIB_DIR)/graphics_utils.cmo $(LIB_DIR)/nfa.cmo $(LIB_DIR)/dfa.cmo $(LIB_DIR)/regex.cmo $(LIB_DIR)/dfa_min.cmo $(LIB_DIR)/gnfa.cmo  $(LIB_DIR)/automaton_drawing.cmo $(SRC_DIR)/lexer.cmo $(SRC_DIR)/parser.cmo $(SRC_DIR)/main.cmo 

main.cmo:
	$(OCAMLBUILD) -c $(SRC_DIR)/main.ml -I $(LIB_DIR) -I $(SRC_DIR)


lexer.ml: lexer.mll
	$(OCAMLLEX) $(SRC_DIR)/lexer.mll -o $(SRC_DIR)/lexer.ml

parser.ml parser.mli:
	$(OCAMLPARSER) -b $(SRC_DIR)/parser $(SRC_DIR)/parser.mly

parser.cmi: parser.mli
	$(OCAMLBUILD) -c $(SRC_DIR)/parser.mli -I $(LIB_DIR) -I $(SRC_DIR)

lexer.cmo:
	$(OCAMLBUILD) -c $(SRC_DIR)/lexer.ml -I $(LIB_DIR) -I $(SRC_DIR)

parser.cmo:
	$(OCAMLBUILD) -c $(SRC_DIR)/parser.ml -I $(LIB_DIR) -I $(SRC_DIR)


%.cmi:
	$(OCAMLBUILD) -c -I $(LIB_DIR) $(LIB_DIR)/$*.mli

%.mll:
	$(OCAMLLEX) ${SRC_DIR}/$*.mll


%.cmo: %.cmi
	$(OCAMLBUILD) -c -I $(LIB_DIR) $(LIB_DIR)/$*.ml 

clean:
	rm -f sortie $(LIB_DIR)/*.cmi $(LIB_DIR)/*.cmo $(SRC_DIR)/*.cmo $(SRC_DIR)/*.cmi $(SRC_DIR)/parser.mli $(SRC_DIR)/parser.ml $(SRC_DIR)/lexer.ml $(SRC_DIR)/parser.cmi

.PHONY: clean

#ocamllep