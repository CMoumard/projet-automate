let () =
  set_binary_mode_out stdout true;

Printf.printf "|-------------------------------|\n";
Printf.printf "|                               |\n";
Printf.printf "|   Convertisseur interactif    |\n";
Printf.printf "|        d'automates            |\n";
Printf.printf "|                               |\n";
Printf.printf "|-------------------------------|\n";

Printf.printf "Que souhaitez-vous faire ?\n";
Printf.printf "1. Convertir une expression régulière en NFA\n";
Printf.printf "2. Convertir une expression régulière en DFA\n";
Printf.printf "3. Convertir une expression régulière en DFA minimal\n";
Printf.printf "4. Convertir un NFA en expression régulière\n";


let rec choix_option () : int = 
  Printf.printf "Votre choix : ";
  let choix = read_int() in
  if choix = 1 || choix = 2 || choix = 3 || choix = 4 then
    choix
  else(
    Printf.printf "Choix invalide\n";
    choix_option ()
  );
in
let choix = choix_option () in
let regex_reconnue = ref Regex.Epsilon in
let choix_123_Graphics = ref false in
if choix = 1 || choix = 2 || choix = 3 then
(
  let reconnue = ref false in
  
  while not !reconnue do
    Printf.printf "Entrez une expression régulière : ";
    flush stdout;
    try
      let lexbuf = Lexing.from_channel stdin in
        let regex = Parser.ligne Lexer.token lexbuf in
        flush stdout;
        regex_reconnue := regex;
        reconnue := true;
    with Stdlib.Parsing.Parse_error ->
      Printf.printf "Expression régulière non reconnue\n";
  done;

  Printf.printf "Expression régulière reconnue : %s \n" (Regex.regexp_to_string !regex_reconnue);
  flush stdout;
  Printf.printf "Souhaitez-vous : \n";
  Printf.printf "1. Visualiser l'automate sur l'interface Graphics ? 
  /!\\ Cette option est encore expérimentale, et présente vite ses limites à cause de mauvais choix potentiel de positionnement des états. \n";
  Printf.printf "2. Visualiser l'automate avec Graphviz ? L'automate sera sauvegardé dans un fichier .dot et .jpg \n";
  let choix_valide = ref true in
  while !choix_valide do  
    Printf.printf "Votre choix : ";
    let choix = read_int() in 
    if choix = 1 then (
      choix_123_Graphics := true;
      choix_valide := false;
    )
    else if choix = 2 then (
      choix_valide := false;
    )
    else (
      Printf.printf "Choix invalide\n";
    );
  done;
);


if choix = 1 then
  (*Cas regex -> nfa*)
  begin
    let nfa = Nfa.thompson !regex_reconnue in
    if !choix_123_Graphics then
      Automaton_drawing.dessiner_nfa nfa
    else (
      Printf.printf "Entrez un nom de fichier pour sauvegarder l'automate : ";
      let nom_fichier = read_line() in
      Automaton_drawing.nfa_to_dot nfa (nom_fichier ^ ".dot");
      Automaton_drawing.dot_to_jpg (nom_fichier ^ ".dot") (nom_fichier ^ ".jpg");
    );
  end
else if choix = 2 then
  (*Cas regex -> dfa*)
  begin
    let nfa = Nfa.thompson !regex_reconnue in
    let dfa = Dfa.nfa_to_dfa nfa in
    let dfa_to_nfa = Dfa.dfa_to_nfa dfa in
    if !choix_123_Graphics then
      Automaton_drawing.dessiner_nfa nfa
    else (
      Printf.printf "Entrez un nom de fichier pour sauvegarder l'automate : ";
      let nom_fichier = read_line() in
      Automaton_drawing.nfa_to_dot dfa_to_nfa (nom_fichier ^ ".dot");
      Automaton_drawing.dot_to_jpg (nom_fichier ^ ".dot") (nom_fichier ^ ".jpg");
    );
      
  end
else if choix = 3 then
  (*Cas regex -> dfa_min*)
  let nfa = Nfa.thompson !regex_reconnue in
  let dfa = Dfa.nfa_to_dfa nfa in
  let dfa_min = Dfa_min.minimize_dfa dfa in
  let dfa_min_to_nfa = Dfa_min.dfa_min_to_nfa dfa_min in
  begin
    if !choix_123_Graphics then
      Automaton_drawing.dessiner_nfa dfa_min_to_nfa
    else (
      Printf.printf "Entrez un nom de fichier pour sauvegarder l'automate : ";
      let nom_fichier = read_line() in
      Automaton_drawing.nfa_to_dot dfa_min_to_nfa (nom_fichier ^ ".dot");
      Automaton_drawing.dot_to_jpg (nom_fichier ^ ".dot") (nom_fichier ^ ".jpg");
    );
  end
else if choix = 4 then
  (*Cas nfa -> regex*)
  begin
    Automaton_drawing.dessiner_manuellement_dfa();
  end
else
  begin
    Printf.printf "Choix invalide\n";
  end;;

