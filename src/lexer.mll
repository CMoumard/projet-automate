{
open Parser        (* The type token is defined in parser.mli *)
exception Eof
}
rule token = parse
    [' ' '\t']     { token lexbuf }     (* skip blanks *)
  | ['\n']       { EOL }
  | '('            { LPAREN }
  | ')'            { RPAREN }
  | ['a'-'z''A'-'Z'] as lxm { SYMBOLE (lxm) }
  | '|'            { UNION }
  | '*'            { STAR }
  | eof            { raise Eof }