%{
%}
%type<Regex.regexp> S E T F
%token<char> SYMBOLE
%token EOL
%token UNION STAR LPAREN RPAREN
%left  STAR UNION
%type <Regex.regexp> ligne
%start ligne
%%
ligne: S EOL        { $1 } 
 | EOL              { Regex.Empty }
 ;

S: S UNION E        { Regex.Union($1, $3) }
 | E                { $1 }
 ;

E: E T              { Regex.Concat($1, $2)}
 | T                {  $1 }
 ;

T: T STAR           { Regex.Closure $1 }
 | F                { $1 }
 ;

F: LPAREN S RPAREN       { $2 }
 | SYMBOLE          { Regex.Char $1 }
 ;

%%
