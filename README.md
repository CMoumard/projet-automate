# Projet automate
### 
Ce dépôt apporte une librairie permettant de réaliser des conversions sur des automates finis entre expressions régulières. Pour transformer une expression régulière en automate d'état fini non déterministe (`NFA`), elle utilise l'[algorithme de Thompson](https://fr.wikipedia.org/wiki/Algorithme_de_Thompson). Pour transformer un automate fini non déterministe (`NFA`) en automate fini déterministe (`DFA`), elle utilise l'[algorithme de construction par sous-ensembles](https://fr.wikipedia.org/wiki/Construction_par_sous-ensembles). Enfin, pour transformer un automate fini déterministe (`DFA`) en automate fini déterministe minimal, elle utilise l'[algorithme de Moore](https://fr.wikipedia.org/wiki/Algorithme_de_Moore_de_minimisation_d%27un_automate_fini).


### Fonctionnalités
* Conversion d'une expression régulière en automate fini non déterministe (`NFA`).
* Conversion d'un automate fini non déterministe (`NFA`) en automate fini déterministe (`DFA`).
* Conversion d'un automate fini déterministe (`DFA`) en automate fini déterministe minimal.
* Conversion d'un automate fini non déterministe (`NFA`) en expression régulière par l'utilisation d'automate fini non déterministe généralisé (`GNFA`).

La visualisation des automates est permise via une image au format `JPG` généré à partir d'un `DOT` grâce à la librairie `Graphviz`. La création manuelle d'un NFA est assurée par la librairie `Graphics`.

### Installation
Pour installer ce projet, il suffit de lancer les commandes suivante :
```bash
$ git clone https://gitlab.com/CMoumard/projet-automate.git
$ cd projet-automate
$ opam install graphics
$ opam install graphviz
$ make
```
Il faudra bien sûr avoir installé `opam` et `ocaml` sur votre machine.

### Visualisation


Création manuelle d'un automate fini non déterministe (`NFA`):
![Interface de création de NFA](images/interface_dessiner_nfa.jpg)



Conversion de la l'expression régulière `a*b*` en automate fini non déterministe (`NFA`):
![NFA de la regex `a*b*`](images/automate_nfa.jpg)

Conversion de l'automate fini non déterministe (`NFA`) en automate fini déterministe minimal:
![DFA minimal de la regex `a*b*`](images/automate_dfa.jpg)