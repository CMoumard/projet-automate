module RegexMap : Map.S with type key = Regex.regexp

type transitions_gnfa = Fsm.StateSet.t RegexMap.t

(* Type pour représenter un automate fini déterministe *)
type gnfa = {
  start : Fsm.state;
  (** l'état de départ de l'automate *)

  finals: Fsm.state;
  (** les états finaux (ou "acceptants") de l'automate *)

  all_states: Fsm.StateSet.t;
  (** l'ensemble de tous les états de l'automate *)

  alphabet: Regex.regexp list;
  (** l'alphabet de l'automate *)

  delta: Fsm.state -> transitions_gnfa;
  (** la fonction de transition, qui associe un état et un caractère à un état *)
}



(** [nfa_to_regex nfa] est l'expression régulière qui reconnaît les mêmes chaînes que l'automate [nfa] *)
val gnfa_to_regex : gnfa -> Regex.regexp


(** [gnfa_to_regex gnfa] est l'expression régulière qui reconnaît les mêmes chaînes que l'automate [gnfa] *)
val nfa_to_gnfa : Nfa.nfa -> gnfa


val supprimer_etat : gnfa -> gnfa

val union_etats : gnfa -> gnfa

val print_gnfa : gnfa -> unit
