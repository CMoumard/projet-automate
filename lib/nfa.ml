type nfa = {
  start : Fsm.StateSet.t;
  (** the start states *)

  finals: Fsm.StateSet.t;
  (** the final (or "accept") states *)

  all_states: Fsm.StateSet.t;
  (** the set of all states in the NFA *)

  alphabet : Fsm.alphabet;
  (** the alphabet of the NFA *)

  delta: Fsm.state -> Fsm.transitions;
}


let rec thompson (r : Regex.regexp) : nfa =
  match r with
  | Empty ->
    (*Ici, nous traitons le cas de l'expression régulière vide. Comme cette expression ne reconnaît aucune chaîne, le nfa qu'on construit doit être un automate qui ne possède aucun état de départ ni d'arrivée.*)
              { start = Fsm.StateSet.empty;
              finals = Fsm.StateSet.empty;
              all_states = Fsm.StateSet.empty;
              delta = (fun _ -> Fsm.CharMap.empty); 
              alphabet = []
              }
  | Epsilon ->
    (*Ici, nous avons créé un automate avec deux états : un état de départ (l'état 0) et un état d'arrivée (l'état 1). Nous avons défini l'état 0 comme état de départ et l'état 1 comme état d'arrivée. Nous avons également défini une transition entre l'état 0 et l'état 1, en passant par l'épsilon transition (qui est représentée par le caractère '\000'). Cela permet à l'automate de passer directement de l'état de départ à l'état d'arrivée, sans avoir à lire aucun caractère, ce qui correspond à la reconnaissance d'une chaîne vide.*)
    { start = Fsm.StateSet.singleton 0l;
      finals = Fsm.StateSet.singleton 1l;
      all_states = Fsm.StateSet.of_list [0l; 1l];
      delta = (fun s -> if s = 0l then
                          Fsm.CharMap.singleton '\000' (Fsm.StateSet.singleton 1l)
                        else
                          Fsm.CharMap.empty); 
      alphabet = []
    }
  | Char c -> 
    (*Pour l'opération Char c, nous voulons que l'automate reconnaisse uniquement le caractère c. Pour cela, nous créons un automate qui possède deux états : un état de départ et un état d'arrivée. Nous définissons également une transition normale de l'état de départ vers l'état d'arrivée, en passant par le caractère c.*)
    { 
    
    start = Fsm.StateSet.singleton 0l;
    finals = Fsm.StateSet.singleton 1l;
    all_states = Fsm.StateSet.of_list [0l; 1l];
    delta = (fun s -> if s = 0l then 
                        Fsm.CharMap.singleton c (Fsm.StateSet.singleton 1l)
                      else 
                        Fsm.CharMap.empty); 
    alphabet = [c]
    }
  | Union (r1, r2) ->
    (*Ici, nous traitons l'opération de union de deux expressions régulières. Pour cela, nous créons d'abord les nfas correspondant à chacune des deux expressions régulières en utilisant récursivement la fonction thompson. Ensuite, nous créons un nouveau nfa qui possède nouvel un état de départ et d'arrivée. La fonction de transition de ce nouvel automate possède une transition à partir de l'état de départ, qui est une épsilon-transition vers l'ensemble des états de départ des deux nfas précédents. Elle possède également une autre transition de l'ensemble des états d'arrivés des deux nfas précédent vers un nouvel état final. *)
    let nfa1 = thompson r1 in
    let nfa2 = thompson r2 in
    (*On modifie ici les noms des états de nfa2 afin qu'ils soient indexés après le nombre d'états de nfa1 (et ainsi éviter les conflits)*)
    let nfa2' = { start = Fsm.StateSet.map (fun s -> Int32.add s (Int32.of_int (Fsm.StateSet.cardinal nfa1.all_states))) nfa2.start;
                  finals = Fsm.StateSet.map (fun s -> Int32.add s (Int32.of_int (Fsm.StateSet.cardinal nfa1.all_states))) nfa2.finals;
                  all_states = Fsm.StateSet.map (fun s -> Int32.add s (Int32.of_int (Fsm.StateSet.cardinal nfa1.all_states))) nfa2.all_states;
                  alphabet = nfa2.alphabet;
                  delta = (fun s -> 
                    Fsm.CharMap.map 

                      (fun states -> Fsm.StateSet.map (fun s -> Int32.add s (Int32.of_int (Fsm.StateSet.cardinal nfa1.all_states))) states)  
                      
                      (nfa2.delta (Int32.sub s (Int32.of_int (Fsm.StateSet.cardinal nfa1.all_states)))))
                  } in
    let start = Fsm.StateSet.singleton (Int32.of_int (Fsm.StateSet.cardinal nfa1.all_states + Fsm.StateSet.cardinal nfa2'.all_states)) in
    let finals = Fsm.StateSet.singleton (Int32.of_int (Fsm.StateSet.cardinal nfa1.all_states + Fsm.StateSet.cardinal nfa2'.all_states + 1)) in
    let all_states = Fsm.StateSet.union nfa1.all_states nfa2'.all_states in
    let all_states = Fsm.StateSet.union all_states start in
    let all_states = Fsm.StateSet.union all_states finals in
    { 
      start = start;
      finals = finals;
      all_states = all_states;
      delta = (fun s -> (*Si s est dans un état de départ*)
                        if Fsm.StateSet.mem s start then
                          Fsm.CharMap.singleton '\000' (Fsm.StateSet.union nfa1.start nfa2'.start)
                        (*Sinon si s est dans un état final de nfa1**)
                        else if Fsm.StateSet.mem s nfa1.finals then
                          Fsm.CharMap.singleton '\000' (finals)
                        (*Sinon si s est dans un état final de nfa2**)
                        else if Fsm.StateSet.mem s nfa2'.finals then
                          Fsm.CharMap.singleton '\000' (finals)
                        (*Sinon si s est dans un état de nfa1**)
                        else if Fsm.StateSet.mem s nfa1.all_states then
                          nfa1.delta s
                        (*Sinon si s est dans un état de nfa2**)
                        else if Fsm.StateSet.mem s nfa2'.all_states then
                          nfa2'.delta s
                        else
                          Fsm.CharMap.empty);
      alphabet = Fsm.union nfa1.alphabet nfa2'.alphabet
    }


  | Concat (r1, r2) ->
    (*Pour l'opération de concaténation de deux expressions régulières, nous voulons que l'automate reconnue des chaînes de la forme s1s2 où s1 est reconnu par l'expression régulière r1 et s2 par l'expression régulière r2. Pour cela, nous devons créer un automate qui commence par lire s1 et qui, une fois s1 lue, passe à la lecture de s2.

    Pour créer cet automate, nous utilisons les nfas des deux expressions régulières r1 et r2 que nous avons créés précédemment. Nous définissons l'état de départ de notre automate comme étant l'état de départ de r1, et nous définissons les états d'arrivée de notre automate comme étant les états d'arrivée de r2.

    Pour la fonction de transition de notre automate, nous voulons qu'elle utilise la fonction de transition de r1 sauf pour les états d'arrivée de r1 qui sont aussi des états de départ de r2. Pour ces états, nous voulons qu'il y ait une épsilon-transition vers les états de départ de r2. Cela permet à l'automate de passer de la lecture de s1 à la lecture de s2 sans avoir à lire de caractère supplémentaire.*)
     let nfa1 = thompson r1 in
     let nfa2 = thompson r2 in
    (*On modifie ici les noms des états de nfa2 afin qu'ils soient indexés après le nombre d'états de nfa1 (et ainsi éviter les conflits)*)
    let nfa2' = 
                { start = Fsm.StateSet.map (fun s -> Int32.add s (Int32.of_int (Fsm.StateSet.cardinal nfa1.all_states))) nfa2.start;
                finals = Fsm.StateSet.map (fun s -> Int32.add s (Int32.of_int (Fsm.StateSet.cardinal nfa1.all_states))) nfa2.finals;
                all_states = Fsm.StateSet.map (fun s -> Int32.add s (Int32.of_int (Fsm.StateSet.cardinal nfa1.all_states))) nfa2.all_states;
                alphabet = nfa2.alphabet;
                delta = (fun s -> 
                  Fsm.CharMap.map 

                    (fun states -> Fsm.StateSet.map (fun s -> Int32.add s (Int32.of_int (Fsm.StateSet.cardinal nfa1.all_states))) states)  
                    
                    (nfa2.delta (Int32.sub s (Int32.of_int (Fsm.StateSet.cardinal nfa1.all_states)))))
                } in

     { 
      start = nfa1.start;
      finals = nfa2'.finals;
      all_states = Fsm.StateSet.union nfa1.all_states nfa2'.all_states;
      alphabet = Fsm.union nfa1.alphabet nfa2'.alphabet;
      delta = (fun s -> if Fsm.StateSet.mem s nfa1.finals then
                          Fsm.CharMap.singleton '\000' nfa2'.start
                        (*Si s est dans un état de nfa1*)
                        else if Fsm.StateSet.mem s nfa1.all_states then
                          nfa1.delta s
                        (*Sinon si s est dans un état de nfa2*)
                        else if Fsm.StateSet.mem s nfa2'.all_states then
                          nfa2'.delta s
                        else
                          Fsm.CharMap.empty);
      }
  | Closure r ->
    (* Pour l'opération de fermeture de Kleene, nous voulons que l'automate puisse lire une occurrence de r, puis passer à la lecture de la prochaine occurrence sans avoir à lire de caractère supplémentaire. Pour cela, nous créons un automate qui possède deux nouveaux états : un état de départ et un état d'arrivée. Nous définissons également une épsilon-transition de l'état de départ vers l'ensemble des états de départ de l'automate de r, et une épsilon-transition de chaque état d'arrivée de l'automate de r vers l'état d'arrivée de l'automate de fermeture de Kleene. Cela permet à l'automate de passer de la lecture d'une occurrence de r à la lecture de la suivante sans avoir à lire de caractère supplémentaire.

    De plus, nous voulons que l'automate puisse accepter une chaîne vide, c'est-à-dire ne pas lire d'occurrence de r. Pour cela, nous définissons une épsilon-transition du nouvel état de départ vers le nouvel état d'arrivée de l'automate de fermeture de Kleene. Cela permet à l'automate de reconnaître la chaîne vide sans avoir à lire de caractère supplémentaire.

    Pour les autres états de l'automate de fermeture de Kleene, nous utilisons simplement la fonction de transition de l'automate de r.*)
    let nfa = thompson r in
    let start = Fsm.StateSet.singleton (Int32.of_int (Fsm.StateSet.cardinal nfa.all_states)) in
    let finals = Fsm.StateSet.singleton (Int32.add (Int32.of_int (Fsm.StateSet.cardinal nfa.all_states)) 1l) in
    let all_states = Fsm.StateSet.union (Fsm.StateSet.union start finals) nfa.all_states in
    { 
    start = start;
    finals = finals;
    all_states = all_states;
    alphabet = nfa.alphabet;
    delta = (fun s -> if Fsm.StateSet.mem s start then
                        Fsm.CharMap.singleton '\000' (Fsm.StateSet.union nfa.start finals)
                      else if Fsm.StateSet.mem s nfa.finals then
                        Fsm.CharMap.singleton '\000' (Fsm.StateSet.union finals nfa.start)
                      else if Fsm.StateSet.mem s nfa.all_states then
                        nfa.delta s
                      else
                        Fsm.CharMap.empty);
    }
;;




let rec epsilon_closure (q: Fsm.state) (nfa: nfa) : Fsm.StateSet.t =
  (* Déclare une fonction récursive "epsilon_closure'" qui prend en entrée un état "q", un NFA "nfa" et un ensemble "acc" d'états.
     Cette fonction renvoie l'ensemble des états atteignables en suivant des transitions epsilon depuis "q" dans le NFA "nfa", y compris
     les états de "acc".
   *)
  let visited = ref Fsm.StateSet.empty in
  let rec epsilon_closure' (q: Fsm.state) (nfa: nfa) (acc: Fsm.StateSet.t) : Fsm.StateSet.t =
    (* Si l'état a déjà été visité, renvoie l'ensemble "acc" *)
    if Fsm.StateSet.mem q !visited then acc
    else begin
      visited := Fsm.StateSet.add q !visited;
      (* Récupère les transitions depuis l'état "q" dans le NFA. *)
      let transitions = nfa.delta q in
      (* Récupère les transitions epsilon depuis l'état "q" dans le NFA, s'il y en a. *)
      let epsilons = Fsm.CharMap.find_opt '\000' transitions in
      (* Si il n'y a pas de transitions epsilon depuis "q", renvoie l'ensemble "acc". *)
      match epsilons with
      | None -> acc
      | Some epsilons ->
        (* Sinon, calcule l'union de "acc" et de l'ensemble des états atteignables en suivant les transitions epsilon depuis "q". *)
        let new_acc = Fsm.StateSet.union acc epsilons in
        (* Applique récursivement "epsilon_closure'" à chaque état atteignable depuis "q" en suivant une transition epsilon. *)
        let new_acc = Fsm.StateSet.fold (fun q acc -> epsilon_closure' q nfa acc) epsilons new_acc in
        (* Renvoie l'ensemble obtenu. *)
        new_acc
    end
  in
  (* Appelle la fonction récursive avec l'état "q" et l'ensemble contenant seulement "q" comme ensemble initial. *)
  epsilon_closure' q nfa (Fsm.StateSet.singleton q)
;;

(* Ajoute un état à l'automate "nfa" de numéro q. *)
let add_state (nfa : nfa) (q : Fsm.state) : nfa =
  { nfa with all_states = Fsm.StateSet.add q nfa.all_states }

(* Ajoute une transition à l'automate "nfa" depuis l'état "q1" vers l'état "q2" en lisant le caractère "c". *)
let add_transition (nfa : nfa) (q1 : Fsm.state) (q2 : Fsm.state) (c : char) : nfa =
  let transitions = nfa.delta q1 in
  let transitions = Fsm.CharMap.add c (Fsm.StateSet.add q2 (Fsm.CharMap.find_opt c transitions |> Option.value ~default:Fsm.StateSet.empty)) transitions in
  { nfa with delta = (fun q -> if q = q1 then transitions else nfa.delta q) }

(* Ajoute un état initial à l'automate "nfa" de numéro "q". *)
let add_start_state (nfa : nfa) (q : Fsm.state) : nfa =
  { nfa with start = Fsm.StateSet.add q nfa.start }

(* Ajoute un état final à l'automate "nfa" de numéro "q". *)
let add_final_state (nfa : nfa) (q : Fsm.state) : nfa =
  { nfa with finals = Fsm.StateSet.add q nfa.finals }

(* Ajoute un caractère à l'alphabet de l'automate "nfa". *)
let add_char (nfa : nfa) (c : char) : nfa =
  { nfa with alphabet = List.append nfa.alphabet [c]}

let remove_state (nfa : nfa) (state : Fsm.state) : nfa =
  let all_states = Fsm.StateSet.remove state nfa.all_states in
  let start = Fsm.StateSet.remove state nfa.start in
  let finals = Fsm.StateSet.remove state nfa.finals in
  (*supprimer l'état de toutes les transitions (sortante et incidante)*)
  let delta = (fun q ->
      if q = state then
        Fsm.CharMap.empty
      else
        let transitions = nfa.delta q in
        let transitions = Fsm.CharMap.map (fun states -> Fsm.StateSet.remove state states) transitions in
        transitions
    ) in
  { all_states; start; finals; delta; alphabet = nfa.alphabet }

let print_nfa nfa =
  (* Afficher les états initiaux *)
  print_string "Start states: {";
  Fsm.StateSet.iter (fun s -> Printf.printf "%ld, " s) nfa.start;
  print_string "}\n";

  (* Afficher les états finaux *)
  print_string "Final states: {";
  Fsm.StateSet.iter (fun s -> Printf.printf "%ld, " s) nfa.finals;
  print_string "}\n";

  (* Afficher les transitions *)
  print_string "Transitions:\n";
  Fsm.StateSet.iter (fun s ->
      (* Pour chaque état, itérer sur les transitions sortantes *)
      let transitions = nfa.delta s in
      Fsm.CharMap.iter (fun c states ->
          (* Pour chaque caractère de transition, afficher la transition *)
          Printf.printf "%ld --" s;
          if c = '\000' then
            print_string "\\epsilon"
          else
            print_char c;
          print_string "--> {";
          Fsm.StateSet.iter (fun s' -> Printf.printf "%ld, " s') states;
          print_string "}\n") transitions) nfa.all_states;
  print_newline ();
;;


