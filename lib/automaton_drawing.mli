val dessiner_nfa : Nfa.nfa -> unit
(** [draw_nfa nfa] dessine l'automate [nfa] à l'écran *)


val nfa_to_dot : Nfa.nfa -> string -> unit
(** [nfa_to_dot nfa] permet de convertir un automate [nfa] en un fichier DOT ayant le nom donné en paramètre *)

val dot_to_jpg : string -> string -> unit
(** [dot_to_jpg nom] permet de convertir un fichier DOT [nom1] en un fichier JPG ayant le nom [nom2] *)

val dessiner_manuellement_dfa : unit -> unit
(** [dessiner_manuellement_dfa] permet de dessiner un automate à l'aide de la souris *)