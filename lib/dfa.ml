module IntMap = Map.Make(Int32)

(*Les états d'un DFA sont représenté par des ensembles d'états afin de faciliser l'opération de nfa -> dfa (par l'algorithme de construction par sous-ensembles)*)
type dfa = {
  (* l'état de départ de l'automate *)
  start : Fsm.StateSet.t;

  (* les états finaux (ou "acceptants") de l'automate *)
  finals: Fsm.StateSet.t list;

  (* l'ensemble de tous les états de l'automate *)
  all_states: Fsm.StateSet.t list;

  (* l'alphabet de l'automate *)
  alphabet: Fsm.alphabet;

  (* la fonction de transition, qui associe un état et un caractère à un état *)
  delta: Fsm.StateSet.t -> Fsm.transitions
}


let nfa_to_dfa (nfa: Nfa.nfa) : dfa =
  (* L'état de départ du DFA est l'épsilon-fermeture des états de départ du NFA. *)
  let start = Fsm.StateSet.fold 
          (fun q acc -> Fsm.StateSet.union (Nfa.epsilon_closure q nfa) acc) 
            nfa.Nfa.start Fsm.StateSet.empty in
  (* L'ensemble de tous les états du DFA contenant initialement l'état puit. *)
  let all_states = ref [] in
  (* L'ensemble des états finaux du DFA est initialement vide. *)
  let finals = ref [] in
  (* La fonction de transition du DFA est initialement vide. *)
  let delta = ref (fun _ -> Fsm.CharMap.empty) in
  (* L'ensemble des états qui n'ont pas encore été traités. Initialement, il contient seulement l'ensemble start.*)
  let to_do = ref [start; Fsm.StateSet.empty] in
  (* Tant qu'il y a des états qui n'ont pas encore été traités... *)
  while (!to_do) != [] do
    (* Récupère un état non traité. *)
    let q = List.hd !to_do in
    (* Supprime de l'ensemble des états non traités. *)
    to_do := List.tl !to_do;
    (* On l'ajoute à l'ensemble de tous les états. *)
    all_states := q::!all_states;
    (* Si q contient un entier qui est un état final dans le Nfa, on ajoute l'état q comme état final. *)
    if (Fsm.StateSet.exists (fun p -> Fsm.StateSet.mem p nfa.Nfa.finals) q) then
      finals := q::!finals;
    (* Pour chaque caractère c... *)
    List.iter(fun c ->
      (* Calcule l'ensemble des états atteignables à partir de chaque état de q sur c. *)
      let r = Fsm.StateSet.fold (fun p acc -> Fsm.StateSet.union (Nfa.epsilon_closure p nfa) acc) 
        (*calcule tous les états accessible à partir d'une transition sur tous les états de q avec c.*)
        (Fsm.StateSet.fold (fun p acc -> 
          match Fsm.CharMap.find_opt c (nfa.Nfa.delta p) with
          | Some s -> Fsm.StateSet.union s acc
          | None -> acc) q Fsm.StateSet.empty)
        Fsm.StateSet.empty
      in
      (* Si cet ensemble n'est pas vide *)
      let not_empty = not (Fsm.StateSet.is_empty r) in
      (* Si cet ensemble n'est pas dans all_states*)
      let not_in_all_states = not (List.exists (Fsm.StateSet.equal r) !all_states) in
      (* Si cet ensemble n'est pas dans to_do *)
      let not_in_to_do = not (List.exists (Fsm.StateSet.equal r) !to_do) in

      if not_empty then ( 
        (*On récupère l'ensemble des transitions précédentes afin de les mettres à jour*)
        let delta_temp = !delta in
        (* On ajoute la transition (q, c) -> r à la fonction de transition du DFA. *)
        delta := (fun q' -> 
          if Fsm.StateSet.equal q q' then  
            Fsm.CharMap.add c r (delta_temp q') 
          else 
            (delta_temp q') 
        );
        (* Si cette transition n'a pas déjà été vue, on l'ajoute*)
        if not_in_all_states && not_in_to_do then (
          to_do := r::!to_do;
        );
      ) else (
        let delta_temp = !delta in
        delta := (fun q' -> 
          if Fsm.StateSet.equal q q' then  
            Fsm.CharMap.add c Fsm.StateSet.empty (delta_temp q') 
          else 
            (delta_temp q')
        );
      );
    ) nfa.Nfa.alphabet;
  done;
  (* Renvoie le DFA. *)
  { start; finals = !finals; all_states = !all_states;
  alphabet = nfa.Nfa.alphabet;
  delta = !delta }
;;


let dfa_to_nfa : dfa -> Nfa.nfa = fun dfa ->
  let rec get_dict ens_states dict i =
    match ens_states with
    | [] -> dict
    | ens_state::ens_states' -> 
      get_dict ens_states' (IntMap.add i ens_state dict) (Int32.add i 1l)
  in 
  let find_key_by_value map value =
    let key_ref = ref None in
    IntMap.iter (fun k v -> if v = value then key_ref := Some k) map;
    Option.get !key_ref
  in
  let dict = get_dict dfa.all_states IntMap.empty 0l in
  let start = Fsm.StateSet.of_list [find_key_by_value dict dfa.start] in
  let finals = Fsm.StateSet.of_list (List.map (fun s -> find_key_by_value dict s) dfa.finals) in
  let all_states = Fsm.StateSet.of_list (IntMap.bindings dict |> List.map (fun (k, _) -> k)) in
  let delta = fun s -> 
    let ens_state = IntMap.find s dict in
    Fsm.CharMap.map (fun ens_state' -> Fsm.StateSet.of_list [find_key_by_value dict ens_state']) (dfa.delta ens_state)
  in
  {start = start; finals = finals; alphabet = dfa.alphabet; all_states = all_states; delta = delta}

  

let print_dfa (dfa: dfa) =
  (* Affiche l'état de départ de l'automate *)
  print_endline "Start state:";
  Fsm.StateSet.iter (fun q -> Printf.printf "%ld " q) dfa.start;
  print_newline ();
  (* Affiche les états finaux de l'automate *)
  print_endline "Final states:";

  List.iter (fun qs -> Fsm.StateSet.iter (fun q -> Printf.printf "%ld " q) qs; print_newline ()) dfa.finals;
  (*afficher le cardinal de dfa.finals*)
  print_endline "Cardinal of dfa.finals:";
  Printf.printf "%d" (List.length dfa.finals);
  print_newline ();

  (*afficher le cardinal de allstates*)
  print_endline "Cardinal of dfa.all_states:";
  Printf.printf "%d" (List.length dfa.all_states);
  print_newline ();
  
  (*Affiche tous les états*)
  print_endline "All states:";
  List.iter (fun qs -> Fsm.StateSet.iter ( print_string "( " ; fun q -> Printf.printf "%ld " q) qs; print_string " )"; print_newline ()) dfa.all_states;
  print_newline ();

  (*Affiche les transitions*)
  print_endline "Transition table:";
  List.iter (
    fun qs -> 
      Fsm.StateSet.iter (fun q -> Printf.printf "%ld " q) qs;
      print_string " -> ";
      Fsm.CharMap.iter (fun c qs' -> 
        Printf.printf "%c -> " c;
        Fsm.StateSet.iter (fun q -> Printf.printf "%ld " q) qs';
        print_string " | "
      ) (dfa.delta qs);
      print_newline ()
  ) dfa.all_states;
;;