(** Un type pour représenter les expressions régulières *)
type regexp =
  | Empty
  | Epsilon
  | Char of char
  | Union of regexp * regexp
  | Concat of regexp * regexp
  | Closure of regexp

(** [empty] est l'expression régulière qui ne reconnaît aucune chaîne *)
val empty : regexp

(** [epsilon] est l'expression régulière qui ne reconnaît qu'une chaîne vide *)
val epsilon : regexp

(** [character c] est l'expression régulière qui ne reconnaît qu'un caractère [c] *)
val character : char -> regexp

(** [union r1 r2] est l'expression régulière qui reconnaît les chaînes reconnues par [r1] ou [r2] *)
val union : regexp -> regexp -> regexp

(** [concat r1 r2] est l'expression régulière qui reconnaît les chaînes de la forme s1s2 où s1 est reconnue par [r1] et s2 par [r2] *)
val concat : regexp -> regexp -> regexp

(** [closure r] est l'expression régulière qui reconnaît les chaînes de la forme s1s2...sn où chaque si est reconnu par [r] *)
val closure : regexp -> regexp

(** [regexp_to_string r] est la chaîne de caractères représentant l'expression régulière [r] *)
val regexp_to_string : regexp -> string

(* [simplifier r] est l'expression régulière équivalente à [r] mais simplifiée (non au maximum)*)
val simplifier : regexp -> regexp


