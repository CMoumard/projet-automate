module RegexMap = Map.Make(struct
  type t = Regex.regexp
  let compare = compare
end)
type transitions_gnfa = Fsm.StateSet.t RegexMap.t


(* Type pour représenter un automate fini déterministe *)
type gnfa = {
  start : Fsm.state;
  (** l'état de départ de l'automate *)

  finals: Fsm.state;
  (** les états finaux (ou "acceptants") de l'automate *)

  all_states: Fsm.StateSet.t;
  (** l'ensemble de tous les états de l'automate *)

  alphabet: Regex.regexp list;
  (** l'alphabet de l'automate *)

  delta: Fsm.state -> transitions_gnfa;
  (** la fonction de transition, qui associe un état et un caractère à un état *)
}


let nfa_to_gnfa (nfa: Nfa.nfa) : gnfa =
  (*On converti l'alphabet du nfa en ensemble de Regex*)
  let nouveau_alphabet = 
    List.map (fun c -> Regex.Char c) nfa.alphabet in
  let nouveau_delta = fun s -> 
    let set = nfa.delta s in
    (*on converti set, de type Fsm.transitions, en un nouvel "objet" de transitions_gnfa. Autrement dit, on converti les char des transitions en Regex*)
    Fsm.CharMap.fold (fun c t acc -> 
      if c = '\000' then
        RegexMap.add Regex.Epsilon t acc
      else
        RegexMap.add (Regex.Char c) t acc)
        set RegexMap.empty in
  (*On ajoute deux nouvels états: un nouvel état initial, et un nouvel état final*)
  let nb_etats = Fsm.StateSet.cardinal nfa.all_states in
  let nouveau_start = Int32.of_int nb_etats in
  let nouveau_final = Int32.of_int (nb_etats + 1) in
  (*On ajoute des transitions à nouveau_delta: nouveau_start doit pointer vers tous les états de départs de nfa par une epsilon-transition. Tous les états finaux de nfa doit pointer vers l'état nouveau_final.*)
  let nouveau_delta' = fun s ->
    if s = nouveau_start then
      Fsm.StateSet.fold (fun s acc -> RegexMap.add Regex.Epsilon (Fsm.StateSet.singleton s) acc) nfa.start RegexMap.empty
    (*sinon si s est compris dans les états finaux de nfa.finals*)
    else if Fsm.StateSet.mem s nfa.finals then
      (*s'il existe déjà une epsilon-transition, on fait l'union de l'état d'arrivé*)
      let ancien_delta = nouveau_delta s in
      if RegexMap.mem Regex.Epsilon ancien_delta then
        let ancien_etats = RegexMap.find Regex.Epsilon ancien_delta in
        let nouveau_etats = Fsm.StateSet.add nouveau_final ancien_etats in
        RegexMap.add Regex.Epsilon nouveau_etats (nouveau_delta s)
      else
        (*sinon, on ajoute une nouvelle epsilon-transition*)
        RegexMap.add Regex.Epsilon (Fsm.StateSet.singleton nouveau_final) (nouveau_delta s)
    else
      nouveau_delta s
  in
  let nouveau_all_states = Fsm.StateSet.add nouveau_final (Fsm.StateSet.add nouveau_start nfa.all_states) in
  {alphabet = nouveau_alphabet; 
  start = nouveau_start; 
  finals = nouveau_final; 
  all_states = nouveau_all_states; 
  delta = nouveau_delta'}
;;

let union_etats (gnfa: gnfa) : gnfa = 
  let nouveau_delta = ref gnfa.delta in
  Fsm.StateSet.iter (fun etat ->
    let ancien_delta = !nouveau_delta in
    (*On récupère les transitions de l'état*)
    let transitions = ancien_delta etat in
    (*On récupère une liste des états atteignables par transitions. Aucun duplicat dans la liste. Attention, ce doit être un objet de type StateSet*)
    let liste_etats = RegexMap.fold (fun _ t acc -> Fsm.StateSet.union t acc) transitions Fsm.StateSet.empty in

    (*On forme un nouveau transitions_gnfa pour etat*)
    let nouvelle_transition = ref RegexMap.empty in

    (*On itère dans chaque état de liste_etats*)
    Fsm.StateSet.iter( fun v -> 

      (*On fait l'union des Regex qui mènent vers v*)
      let regex_to_v = RegexMap.fold (fun c t acc -> if Fsm.StateSet.mem v t then 
        Regex.Union (c, acc)
      else acc) transitions Regex.Empty in

      let ancien_nouvelle_transition = !nouvelle_transition in
      (*On vérifie si regex_to_v est déjà dans ancien_nouvelle_transition*)
      if RegexMap.mem regex_to_v ancien_nouvelle_transition then (
        (*On ajoute v à l'ensemble des états de ancien_nouvelle_transition*)
        let ancien_etats = RegexMap.find regex_to_v ancien_nouvelle_transition in
        let nouveaux_etats = Fsm.StateSet.add v ancien_etats in
        nouvelle_transition := RegexMap.add regex_to_v nouveaux_etats ancien_nouvelle_transition;
      )
      else
        nouvelle_transition := RegexMap.add regex_to_v (Fsm.StateSet.singleton v) ancien_nouvelle_transition;
    ) liste_etats;
    
    (*On ajoute la nouvelle transition de etat dans nouveau_delta*)
    nouveau_delta := fun s -> if s = etat then !nouvelle_transition else ancien_delta s;
    (*On fait l'union de tous les RegexMap de la liste*)
  ) gnfa.all_states;

  {alphabet = gnfa.alphabet;
  start = gnfa.start;
  finals = gnfa.finals;
  all_states = gnfa.all_states;
  delta = !nouveau_delta}
;;


let print_gnfa gnfa =
  (*afficher tous les états*)
  print_string "all_states: ";
  let _ = Fsm.StateSet.iter (fun s -> Printf.printf "%ld " s) gnfa.all_states in
  print_newline ();
  (*afficher l'alphabet*)
  print_string "alphabet: ";
  Printf.printf " %s " (String.concat " " (List.map (fun c -> Regex.regexp_to_string c ) gnfa.alphabet)); 
  print_newline ();
  (*afficher l'état de départ*)
  print_string "start: ";
  let _ =  Printf.printf "%ld " gnfa.start in
  print_newline ();
  (*afficher les états finaux*)
  print_string "finals: ";
  let _ =  Printf.printf "%ld " gnfa.finals in
  print_newline ();
  (*afficher les transitions*)
  print_string "delta: ";
  List.iter (fun s -> 
    Printf.printf "etat %ld -> " s;
    RegexMap.iter (fun r t -> 
      Printf.printf "%s -> %s " (Regex.regexp_to_string r) (Fsm.StateSet.fold (fun s acc -> Printf.sprintf "%s %ld" acc s) t "")
    ) (gnfa.delta s);
    print_newline ();
  ) (Fsm.StateSet.elements gnfa.all_states);
  print_newline ();
;;

let supprimer_etat (gnfa: gnfa) : gnfa = 
  (*On choisi un état au hasard dans l'ensemble des états gnfa (sans l'état de départ et d'arrivé)*)
  let etat_choisi = Fsm.StateSet.choose (Fsm.StateSet.remove gnfa.start (Fsm.StateSet.remove gnfa.finals gnfa.all_states)) in


  (*On supprime cet état*)
  let nouveau_all_states = Fsm.StateSet.remove etat_choisi gnfa.all_states in
  (*On récupère tous les états entrant dans etat_choisi (avec n'importe quel Regex)*)
  let etats_vers_choisi = Fsm.StateSet.filter (fun s -> RegexMap.exists (fun _ t -> Fsm.StateSet.mem etat_choisi t) (gnfa.delta s)) gnfa.all_states in

  (*On vérifie si etat_choisi est contenu dans etats_sortant_choisi : on défini une variable booléenne*)
  let is_star = Fsm.StateSet.mem etat_choisi etats_vers_choisi in


  (*Si true, on sait que l'on a le Star(R)! On supprime etat_choisi de etats_sortant_choisi et etats_vers_choisi*)
  let etats_vers_choisi = if is_star then Fsm.StateSet.remove etat_choisi etats_vers_choisi else etats_vers_choisi in





  (*Dans la (gnfa.delta etat_choisi), on supprime les Regex qui vont vers lui même*)
  let transitions_de_choisi = RegexMap.empty in
  let transitions_de_choisi_ref = ref transitions_de_choisi in
  RegexMap.iter (fun r t -> 
    (*on vérifie si t est non vide*)
    if not (Fsm.StateSet.is_empty t) then (
      let copie = !transitions_de_choisi_ref in
      (*On ajoute la transition dans transitions_de_choisi*)
      transitions_de_choisi_ref := RegexMap.add r t copie;
    )
  ) (gnfa.delta etat_choisi);



  let regex_star = ref [] in

  RegexMap.iter (fun r t -> 
    if (Fsm.StateSet.mem etat_choisi t) then (
      regex_star := r :: !regex_star;
    )
  ) (gnfa.delta etat_choisi);

  
  (*On ajoute des transitions à nouveau_delta: tous les états allant vers etat_choisi doivent pointer vers tous les états sortant de etat_choisi*)
  let nouveau_delta = fun s ->
    if Fsm.StateSet.mem s etats_vers_choisi then
      (*On récupère la regex sur la transition s -> etat_choisi*)
      let regex = RegexMap.fold (fun r t acc -> if Fsm.StateSet.mem etat_choisi t then r else acc) (gnfa.delta s) Regex.Empty in 

      let transitions_de_s = RegexMap.fold 
      (fun r t acc -> 
        (*on filtre etat_choisi de t*)
        let t' = Fsm.StateSet.filter (fun s -> s <> etat_choisi) t in
        (*On ajoute les états sortant de etat_choisi*)
        RegexMap.add r t' acc
      ) (gnfa.delta s) RegexMap.empty in

      (*On ajoute les transitions de etat_choisi à transitions_de_s*)

      if is_star then
      (
        (*On itère sur tous les transitions sortante de etat_choisi*)
        RegexMap.fold 
          (fun r t acc -> 
              RegexMap.add 
                (Regex.Concat (regex, Regex.Concat(Regex.Closure(List.nth !regex_star 0), r))) 
          t acc)
          !transitions_de_choisi_ref transitions_de_s
      )
      else 
      (
        (*On itère sur tous les transitions sortante de etat_choisi*)
        RegexMap.fold (fun r t acc -> RegexMap.add (Regex.Concat (regex, r)) t acc) !transitions_de_choisi_ref transitions_de_s
      );
    else
      gnfa.delta s
  in
  
  {alphabet = gnfa.alphabet; 
  start = gnfa.start; 
  finals = gnfa.finals; 
  all_states = nouveau_all_states; 
  delta = nouveau_delta}
;;




(** [nfa_to_regex nfa] est l'expression régulière qui reconnaît les mêmes chaînes que l'automate [nfa] *)
let gnfa_to_regex (gnfa1 : gnfa) : Regex.regexp =

  (*On fait l'union dse transitions quand c'est nécessaire*)
  let gnfa = union_etats gnfa1 in
  (*tant que le gnfa à plus que 2 états, on supprime un état*)

  let gnfa = ref (if Fsm.StateSet.cardinal gnfa.all_states > 2 then supprimer_etat gnfa else gnfa) in

  while Fsm.StateSet.cardinal (!gnfa).all_states > 2 do
    gnfa := supprimer_etat (!gnfa);
    gnfa := union_etats (!gnfa);
  done;
  (*On retourne la regex qui est sur la transition de l'état de départ vers l'état d'arrivé*)
  Regex.simplifier
  (RegexMap.fold (fun r t acc -> if Fsm.StateSet.mem (!gnfa).finals t then r else acc) ((!gnfa).delta (!gnfa).start) Regex.Epsilon)
;;