open Fsm
type nfa = {
  start : Fsm.StateSet.t;
  (** the start states *)

  finals: Fsm.StateSet.t;
  (** the final (or "accept") states *)

  all_states: Fsm.StateSet.t;
  (** the set of all states *)

  alphabet: Fsm.alphabet;
  (** the alphabet of the NFA *)

  delta: Fsm.state -> Fsm.transitions;
  (** the transition function, that maps a state and a character to a
      set of states *)
}

val thompson : Regex.regexp -> nfa
(** [thompson r] returns the NFA that recognizes the language of [r] *)


val print_nfa : nfa -> unit
(** [print_nfa nfa] prints the NFA [nfa] to the standard output *)

val epsilon_closure : state -> nfa -> Fsm.StateSet.t
(** [epsilon_closure nfa states] returns the epsilon closure of [states] in the NFA [nfa] *)

val add_state : nfa -> Fsm.state -> nfa
(** [add_state nfa state] returns a new NFA that is the same as [nfa] except that [state] is added to [all_states] *)

val add_transition : nfa -> Fsm.state -> Fsm.state -> char -> nfa
(** [add_transition nfa state transition] returns a new NFA that is the same as [nfa] except that [transition] is added to the transitions of [state] *)

val add_start_state : nfa -> Fsm.state -> nfa
(** [add_start_state nfa state] returns a new NFA that is the same as [nfa] except that [state] is added to [start] *)

val add_final_state : nfa -> Fsm.state -> nfa
(** [add_final_state nfa state] returns a new NFA that is the same as [nfa] except that [state] is added to [finals] *)

val remove_state : nfa -> Fsm.state -> nfa
(** [remove_state nfa state] returns a new NFA that is the same as [nfa] except that [state] is removed from the NFA *)

val add_char : nfa -> char -> nfa
(** [add_char nfa c] returns a new NFA that is the same as [nfa] except that [c] is added to [alphabet] *)


