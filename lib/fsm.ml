type state = int32
module StateSet = Set.Make(Int32)
module CharMap = Map.Make(Char)

(* Transitions d'un autoamte fini*)
type transitions = StateSet.t CharMap.t

(* Alphabet d'un automate fini *)
type alphabet = char list

(* Union de l'alphabet d'un automate fini*)
let union (l1 : alphabet) (l2 : alphabet) : alphabet =
  List.fold_left (fun acc x -> if List.mem x acc then acc else x :: acc) l1 l2
