type partition = Fsm.StateSet.t list

type transitions_dfa_min = (Fsm.StateSet.t list) Fsm.CharMap.t

type dfa_min = {
  start : partition;
  (** l'état de départ de l'automate *)

  finals: partition list;
  (** les états finaux (ou "acceptants") de l'automate *)

  all_states: partition list;
  (** l'ensemble de tous les états de l'automate *)

  alphabet: Fsm.alphabet;
  (** l'alphabet de l'automate *)

  delta: partition -> transitions_dfa_min;
  (** la fonction de transition, qui associe un état et un caractère à un état *)
}


(* Fonction de minimisation d'un automate fini déterministe (DFA) basé sur l'algorithme de Moore *)
val minimize_dfa : Dfa.dfa -> dfa_min


(* Affichage d'un dfa_min *)
val print_dfa_min : dfa_min -> unit


(** Conversion d'une expression régulière en automate d'état fini déterministe minimal*)
val regexp_to_dfa_min : Regex.regexp -> dfa_min




(** Conversion  d'un type DFAmin vers un type NFA*)
val dfa_min_to_nfa : dfa_min -> Nfa.nfa
