(* Type pour représenter les états d'un automate fini non déterministe*)
type state = int32

(* Modules pour représenter des ensembles et des tables de transitions avec des clés de type char *)
module StateSet : Set.S with type elt = int32 
module CharMap : Map.S with type key = char

(* Type pour représenter les transitions d'un automate fini *)
type transitions = StateSet.t CharMap.t

(* Alphabet d'un automate fini *)
type alphabet = char list

(* Union de deux alphabets d'automates fini*)
val union : alphabet -> alphabet -> alphabet
