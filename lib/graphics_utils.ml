(*types pour représenter le graphe (représentant l'automate)*)
type coords = {
    mutable x : float;
    mutable y : float;
}

type sommet = {
    num : Fsm.state;
    mutable position : coords;
    mutable deplacement : coords;
}

type arete = {
    label : char;
    mutable origine : sommet ref;
    mutable destination : sommet ref;
}

type graphe = {
  mutable sommets : (sommet ref) list;
  mutable aretes : (arete ref) list;
}


let placement_etats (nfa: Nfa.nfa) (frame_largeur : int) (frame_hauteur : int) (nb_iterations : int) : graphe =
  (* Fonction qui tire |nfa.all_states| positions aléatoires pour chaque points *)
  let rec tirage_points (nfa: Nfa.nfa) (liste: (sommet ref) list) =
    let all_states = Fsm.StateSet.elements nfa.all_states in
    match all_states with
    | [] -> liste
    | t::q -> 
      let state_set = List.fold_left 
        (fun acc x -> Fsm.StateSet.add x acc) Fsm.StateSet.empty q in
        tirage_points 
          {all_states = state_set; start = nfa.start; finals = nfa.finals; alphabet = nfa.alphabet;  delta = nfa.delta} 
          (ref {num = t; position = {x = 0.; y = 0.}; deplacement = {x = 0.; y = 0.}}::liste)  
  in 
  (* Fonction qui initialise les arêtes du graphe à partir de l'automate *)
  let aretes_init (nfa:Nfa.nfa) (sommets: (sommet ref) list) : (arete ref) list =
    let edges = ref [] in
    List.iter (fun sommet ->
      let sommet_label = !sommet.num in
      let sommet_transitions = nfa.delta sommet_label in
      let sommet_edges = Fsm.CharMap.bindings sommet_transitions in
      List.iter (fun (label, dest_states) ->
        Fsm.StateSet.iter (fun state ->
          let dest_sommet = List.find (fun v -> !v.num = state) sommets in
          edges := ref {label = label; origine = sommet; destination = dest_sommet} :: !edges
        ) dest_states;
      ) sommet_edges
    ) sommets;
    !edges in
  let sommets = tirage_points nfa [] in
  let aretes = aretes_init nfa sommets in
  let graphe = {sommets = sommets; aretes = aretes} in
  (* insérer les sommets circulairement tous les pi/n, avec n le nombre de sommets *)
  let n = List.length sommets in
  let angle = (2.0 *. 3.141592) /. (float_of_int n) in
  List.iteri (fun i sommet ->
    let x = (float_of_int frame_largeur) /. 2.0 +. (200. *. cos (angle *. (float_of_int i))) in
    let y = (float_of_int frame_hauteur) /. 2.0 +. (200. *. sin (angle *. (float_of_int i))) in
    !sommet.position.x <- x;
    !sommet.position.y <- y;
  ) sommets;
graphe;;



(*Cette fonction dessine une flèche unidirectionnelle, partant de l'origine et allant vers destination*)
let draw_arrow (origine: coords) (destination : coords) =
  let angle = atan2 (destination.y -. origine.y) (destination.x -. origine.x) in
  let arrow_size = 10. in
  let points = Array.of_list [(int_of_float destination.x, int_of_float destination.y);                (int_of_float (destination.x -. arrow_size *. cos (angle +. 0.5)), int_of_float (destination.y -. arrow_size *. sin (angle +. 0.5)));                (int_of_float (destination.x -. arrow_size *. cos (angle -. 0.5)), int_of_float (destination.y -. arrow_size *. sin (angle -. 0.5)));                (int_of_float destination.x, int_of_float destination.y)]
  in
  Graphics.set_color Graphics.black;
  Graphics.fill_poly points;
  Graphics.set_color Graphics.black;
  Graphics.moveto (int_of_float origine.x) (int_of_float origine.y);
  Graphics.lineto (int_of_float destination.x) (int_of_float destination.y);
;;

let supprimer_sommet (graphe: graphe) (sommet: sommet ref) : graphe =
  let sommets = List.filter (fun v -> v != sommet) graphe.sommets in
  {sommets = sommets; aretes = graphe.aretes}
;;

let supprimer_aretes (graphe: graphe) (sommet: sommet ref) : graphe =
  let aretes = List.filter (fun a -> !(!a.origine).num != (!sommet).num && !(!a.destination).num != (!sommet).num) graphe.aretes in
  {sommets = graphe.sommets; aretes = aretes}


(** [dessiner_graphe graphe nfa] permet de dessiner le graphe [graphe] représentant l'automate [nfa] *)
let dessiner_graphe (graphe) (nfa: Nfa.nfa) (rayon_sommet : float) =
  (* Dessine les sommets *)
  List.iter (fun sommet ->
    let x = int_of_float !sommet.position.x in
    let y = int_of_float !sommet.position.y in

    (*Modifier la couleur du fond du sommet en blanc*)
    Graphics.set_color Graphics.white;
    Graphics.fill_circle  x y  (int_of_float rayon_sommet);

    (*Modifier la couleur du contour du sommet en noir*)
    Graphics.set_color Graphics.black;
    Graphics.draw_circle x y (int_of_float rayon_sommet);

    if Fsm.StateSet.mem !sommet.num nfa.finals then
      (*Marquer le sommet*)
      Graphics.draw_circle x y (int_of_float (rayon_sommet /. 1.2));
    (*Afficher un label sur le sommet*)
    if Fsm.StateSet.mem !sommet.num nfa.Nfa.start then (
      Graphics.set_color Graphics.black;
      Graphics.moveto (x - 17) (y - 5);
      Graphics.draw_string "Depart";
    ) else (
      Graphics.set_color Graphics.black;
      Graphics.moveto  x y;
      Graphics.draw_string (string_of_int (Int32.to_int !sommet.num));
    );
  ) graphe.sommets;
  (* Dessine les arêtes *)
  List.iter (fun arete ->
    
      let origine = !(!arete.origine).position in
      let destination = !(!arete.destination).position in
      (*Récupérer la position de départ et d'arrivée de l'arête*)
      let upos = ref origine in
      let vpos = ref destination in
      let delta = {x = Float.sub !vpos.x !upos.x; y = Float.sub !vpos.y !upos.y} in
      let delta_norme = Float.sqrt (Float.mul delta.x delta.x +. Float.mul delta.y delta.y) in
      let delta_sur_norme = {x = Float.div delta.x delta_norme; y = Float.div delta.y delta_norme} in
      let delta_sur_norme_x_rayon = {x = Float.mul delta_sur_norme.x rayon_sommet; y = Float.mul delta_sur_norme.y rayon_sommet} in
      let upos = {x = Float.add !upos.x delta_sur_norme_x_rayon.x; y = Float.add !upos.y delta_sur_norme_x_rayon.y} in
      let vpos = {x = Float.sub !vpos.x delta_sur_norme_x_rayon.x; y = Float.sub !vpos.y delta_sur_norme_x_rayon.y} in
      Graphics.moveto (int_of_float upos.x) (int_of_float upos.y);

      (*afficher l'arrête*)
      Graphics.set_color Graphics.black;
      
      draw_arrow upos vpos;
      
      (*Afficher un label sur l'arête*)
      let label_pos = {x = (Float.add upos.x vpos.x) /. 2.; y = (Float.add vpos.y upos.y) /. 2.} in
      (*ajouter un carré blanc à l'endroit du label*)
      Graphics.set_color Graphics.white;
      Graphics.fill_rect (int_of_float label_pos.x) (int_of_float label_pos.y) 10 10;
      (*afficher le label*)
      Graphics.set_color Graphics.black;
      Graphics.moveto (int_of_float label_pos.x) (int_of_float label_pos.y);
      if !arete.label = '0' then
        Graphics.draw_string "eps"
      else
        Graphics.draw_string (String.make 1 !arete.label);
  ) graphe.aretes;
  Graphics.synchronize ();
;; 
