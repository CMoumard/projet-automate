(** Un type pour représenter les expressions régulières *)
type regexp =
  | Empty
  | Epsilon
  | Char of char
  | Union of regexp * regexp
  | Concat of regexp * regexp
  | Closure of regexp

(** [empty] est l'expression régulière qui ne reconnaît aucune chaîne *)
let empty = Empty

(** [epsilon] est l'expression régulière qui ne reconnaît qu'une chaîne vide *)
let epsilon = Epsilon

(** [character c] est l'expression régulière qui ne reconnaît qu'un caractère [c] *)
let character c = Char c

(** [union r1 r2] est l'expression régulière qui reconnaît les chaînes reconnues par [r1] ou [r2] *)
let union r1 r2 = Union (r1, r2)

(** [concat r1 r2] est l'expression régulière qui reconnaît les chaînes de la forme s1s2 où s1 est reconnue par [r1] et s2 par [r2] *)
let concat r1 r2 = Concat (r1, r2)

(** [closure r] est l'expression régulière qui reconnaît les chaînes de la forme s1s2...sn où chaque si est reconnu par [r] *)
let closure r = Closure r


let rec regexp_to_string = function
  | Empty -> "empty"
  | Epsilon -> "epsilon"
  | Char c -> Char.escaped c
  | Union (r1, r2) ->
    "(" ^ regexp_to_string r1 ^ "|" ^ regexp_to_string r2 ^ ")"
  | Concat (r1, r2) ->
    regexp_to_string r1 ^ regexp_to_string r2
  | Closure r ->
    "(" ^ regexp_to_string r ^ ")*"
;;

let rec simplifier = function
| Empty -> Empty
| Epsilon -> Epsilon
| Char c -> Char c
| Union (r1, r2) ->
  let r1' = simplifier r1 in
  let r2' = simplifier r2 in
  (match r1', r2' with
  
  | Empty, _ -> r2'
  | _, Empty -> r1'
  | Epsilon, Epsilon -> Epsilon
  | _, _ -> Union (r1', r2'))
| Concat (r1, r2) ->
  let r1' = simplifier r1 in
  let r2' = simplifier r2 in
  (match r1', r2' with
    | Empty, _ | _, Empty -> Empty
    | Epsilon, r | r, Epsilon -> r
    | _, _ -> Concat (r1', r2'))
| Closure r ->
  let r' = simplifier r in
  (match r' with
    | Empty -> Epsilon
    | Epsilon -> Epsilon
    | _ -> Closure r')
;;

