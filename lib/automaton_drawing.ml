let frame_hauteur = 500;;
let frame_largeur = 800;;
let rayon = 40.;;

(* Fonction pour convertir le NFA en un fichier DOT *)
let nfa_to_dot (nfa : Nfa.nfa) (file : string) =
  let oc = open_out file in
  let fmt = Format.formatter_of_out_channel oc in
  (* Ecrire l'entête du fichier DOT *)
  Format.fprintf fmt "digraph NFA {\n";
  (* Ajouter les sommets *)
  Fsm.StateSet.iter (fun s ->
    Format.fprintf fmt "  %ld [label = \"%ld\"];\n" s s;
  ) nfa.all_states;
  (* Ajouter les arêtes *)
  Fsm.StateSet.iter (fun s ->
    Fsm.CharMap.iter (fun c dest_states ->
      Fsm.StateSet.iter (fun dest_state ->
        if c = '\000' then
          Format.fprintf fmt "  %ld -> %ld [label = \"ε\"];\n" s dest_state
        else
          Format.fprintf fmt "  %ld -> %ld [label = \"%c\"];\n" s dest_state c
      ) dest_states
    ) (nfa.delta s)
  ) nfa.all_states;
  (* Ajouter les états de départ et finaux *)
  Format.fprintf fmt "  start [shape=none];\n";
  Fsm.StateSet.iter (fun s ->
    Format.fprintf fmt "  start -> %ld;\n" s
  ) nfa.start;
  Fsm.StateSet.iter (fun s ->
  Format.fprintf fmt " %ld [shape=doublecircle];\n" s
  ) nfa.finals;
  (* Ecrire la fin du fichier DOT *)
  Format.fprintf fmt "}\n";
  close_out oc
;;

let dot_to_jpg (dot_file : string) (jpg_file : string) =
  let cmd = Printf.sprintf "dot -Tjpg -o %s %s" jpg_file dot_file in
  ignore (Sys.command cmd)
;;
  


(*Cette fonction dessine un graphe*)
let dessiner_nfa (nfa: Nfa.nfa) =
  (* Fonction qui permet d'attendre une action de l'utilisateur avant de fermer l'affichage*)
  let rec loop () =
    let e = Graphics.wait_next_event [Graphics.Mouse_motion; Graphics.Key_pressed] in
  
    let mouse_description = Printf.sprintf "Mouse position: %d,%d" e.mouse_x e.mouse_y in
    let key_description = if e.keypressed then Printf.sprintf "Key %c was pressed" e.key else "" in

    (*Clear seulement la zone concernant la zone de texte*)
    Graphics.set_color Graphics.white;
    Graphics.fill_rect 0 0 200 100;
    Graphics.set_color Graphics.black;

    Graphics.moveto 0 100; 
    Graphics.draw_string key_description;
    
    Graphics.moveto 0 0; 
    Graphics.draw_string mouse_description;
    if e.key = 'q' then 
      () 
    else 
      loop ()
  in
  let graphe = Graphics_utils.placement_etats nfa frame_largeur frame_hauteur 50 in 
  let () =
    Graphics.open_graph " 800x600";
    Graphics.set_window_title "NFA";
    (*Afficher chaque état de graph. Si cet état est un état de départ du nfa, alors il faut représenter une flèche qui pointe vers un point du disque (qui n'est donc pas l'origine du cercle). Si cet état est parmi les états finaux, il faut insérer dans ce cercle un autre cercle, ayant le même origine que le cercle de départ mais avec un rayon plus petit *)
    List.iter (fun sommet ->
      Graphics.set_color Graphics.black;
      Graphics.draw_circle (int_of_float !sommet.Graphics_utils.position.x) (int_of_float !sommet.position.y) (int_of_float rayon);
      if Fsm.StateSet.mem !sommet.num nfa.start then
        Graphics_utils.draw_arrow {x = !sommet.position.x +. (rayon *. 2.) ; y = !sommet.position.y}  {x = !sommet.position.x +. rayon ; y = !sommet.position.y};
      if Fsm.StateSet.mem !sommet.num nfa.finals then
        Graphics.draw_circle (int_of_float !sommet.position.x) (int_of_float !sommet.position.y) (int_of_float (rayon /. 1.2));
      Graphics.moveto (int_of_float !sommet.position.x) (int_of_float !sommet.position.y);
      Graphics.draw_string (string_of_int (Int32.to_int !sommet.num));
    ) graphe.Graphics_utils.sommets;

      
    (* Afficher chaque transition de graph. Placer les labels sur les arêtes. Ajouter un cadre blanc avec des bords noirs dans lequel on placera le label. Le début de chaque transition ne doit pas commencer à l'origine du cercle qui représente l'état, mais sur le point sur disque le plus proche de la destination. Chaque transition doit être unidirectionnelle, et doit être une flèche *)

    List.iter (fun edge ->
      let upos = ref !(!edge.Graphics_utils.origine).position in
      let vpos = ref !(!edge.destination).Graphics_utils.position in
      let delta : Graphics_utils.coords = {x = Float.sub !vpos.x !upos.x; y = Float.sub !vpos.y !upos.y} in
      let delta_norme = Float.sqrt (Float.mul delta.x delta.x +. Float.mul delta.y delta.y) in
      let delta_sur_norme : Graphics_utils.coords = {x = Float.div delta.x delta_norme; y = Float.div delta.y delta_norme} in
      let delta_sur_norme_x_rayon : Graphics_utils.coords = {x = Float.mul delta_sur_norme.x rayon; y = Float.mul delta_sur_norme.y rayon} in
      let upos : Graphics_utils.coords = {x = Float.add !upos.x delta_sur_norme_x_rayon.x; y = Float.add !upos.y delta_sur_norme_x_rayon.y} in
      let vpos : Graphics_utils.coords = {x = Float.sub !vpos.x delta_sur_norme_x_rayon.x; y = Float.sub !vpos.y delta_sur_norme_x_rayon.y} in
      Graphics.moveto (int_of_float upos.x) (int_of_float upos.y);
      (*Graphics.lineto (int_of_float vpos.x) (int_of_float vpos.y);*)
      Graphics_utils.draw_arrow upos vpos;
      let label_pos : Graphics_utils.coords = {x = Float.div (Float.add upos.x vpos.x) 2.; y = Float.div (Float.add upos.y vpos.y) 2.} in
      Graphics.moveto (int_of_float label_pos.x) (int_of_float label_pos.y);
      Graphics.set_color Graphics.black;
      Graphics.draw_rect (int_of_float (Float.sub label_pos.x 10.)) (int_of_float (Float.sub label_pos.y 10.)) 20 20;
      Graphics.set_color Graphics.white;
      Graphics.fill_rect (int_of_float (Float.sub label_pos.x 10.)) (int_of_float (Float.sub label_pos.y 10.)) 20 20;
      Graphics.set_color Graphics.black;
      Graphics.moveto (int_of_float (Float.sub label_pos.x 5.)) (int_of_float (Float.sub label_pos.y 5.));
      if !edge.label = '\000' then
        Graphics.draw_string "eps"
      else (
        Graphics.draw_string (String.make 1 !edge.label)
      );
    ) graphe.aretes;
    loop ();
    Graphics.close_graph ();
  in
  ()
;;



(* Cette fonction permet de dessiner un DFA manuellement sur Graphics, et d'obtenir en sortie un objet de type DFA *)
let dessiner_manuellement_dfa () =
  let rayon = 20. in
  let clear_information() =
    (*Effacer la zone d'information*)
    Graphics.set_color Graphics.white;
    Graphics.fill_rect 100 570 600 20;
    (*Insérer des bords noirs*)
    Graphics.set_color Graphics.black;
    Graphics.draw_rect 100 570 600 20;
  in
  let rec loop (index : int32) (graphe : Graphics_utils.graphe) (bool_sommet_selectionne : bool) (sommet_selectionne : Graphics_utils.sommet) (nfa : Nfa.nfa) =
    (*Afficher une aide sur les raccourcis*)
    Graphics.moveto 500 105;
    Graphics.draw_string "Raccourcis: ";

    (*-> ajouter un état*)
    Graphics.moveto 450 85;
    Graphics.draw_string "clic souris puis s: ajouter un etat";

    (*-> ajouter une transition*)
    Graphics.moveto 450 70;
    Graphics.draw_string "clic souris puis t: ajouter une transition";

    (*-> définir un état comme un état de départ*)
    Graphics.moveto 450 55;
    Graphics.draw_string "clic souris puis d: definir un etat comme etat de depart";

    (*-> définir un état comme un état final*)
    Graphics.moveto 450 40;
    Graphics.draw_string "clic souris puis f: definir un etat comme etat final";


    (*-> supprimer un état*)
    Graphics.moveto 450 25;
    Graphics.draw_string "clic souris puis x: supprimer un etat";

    (*-> quitter*)
    Graphics.moveto 450 10;
    Graphics.draw_string "q: quitter";


    clear_information();

    Graphics.moveto 200 573;

    Graphics.set_color Graphics.red;
    Graphics.draw_string "Faites un clic souris puis appuyez sur p pour convertir l'automate en regex";
    

    let e = Graphics.wait_next_event [Graphics.Mouse_motion; Graphics.Key_pressed; Graphics.Button_down] in
  
    let mouse_description = Printf.sprintf "Position de la souris: %d,%d" e.mouse_x e.mouse_y in
    let key_description = if e.keypressed then Printf.sprintf "Touche %c enfoncee" e.key else "" in

    (*Clear seulement la zone concernant la zone de texte au sujet de la position de la souris *)
    Graphics.set_color Graphics.white;
    Graphics.fill_rect 0 0 180 20;
    Graphics.set_color Graphics.black;

    (*Clear seulement la zone concernant la zone de texte au sujet de la touche enfoncée*)
    Graphics.set_color Graphics.white;
    Graphics.fill_rect 0 100 110 20;
    Graphics.set_color Graphics.black;

    (*Afficher le texte pour la touche enfoncée*)
    Graphics.moveto 0 100; 
    Graphics.draw_string key_description;
    
    (*Afficher le texte pour la position de la souris*)
    Graphics.moveto 0 0; 
    Graphics.draw_string mouse_description;



    (*Indice représentant le numéro de l'état à ajouter*)
    let indice = ref index in
    (*Graphe représentant l'automate. Il nous est utile pour avoir la position des sommets sur le graphe.*)
    let graphe = ref graphe in
    (*Nfa dessiné*)
    let nfa = ref nfa in

    (*Indicateur soulignant si un état a été sélectionné l'étape précédente*)
    let bool_sommet_selectionne = ref bool_sommet_selectionne in
    (*Sommet représentant l'état sélectionné*)
    let sommet_selectionne = ref sommet_selectionne in
      
    (*vérifier s'il a appuyé sur la touche 'q'*)
    if e.key = 'q' then () 
    else (
      (*vérifier s'il a fait un clic avec sa souris*)
      if e.button then (
        if e.key = 'p' then (
          clear_information();
          Graphics.moveto 205 573;
          Graphics.set_color Graphics.red;
          let regex = Gnfa.gnfa_to_regex (Gnfa.nfa_to_gnfa !nfa) in
          (*Si le nfa ne possède pas d'état final, on défini regex comme Regex.Empty*)
          let regex = if Fsm.StateSet.is_empty (!nfa.finals) then Regex.Empty else regex in
          (*Si le nfa ne possède pas d'état de départ, on défini regex comme Regex.Empty*)
          let regex = if Fsm.StateSet.is_empty (!nfa.start) then Regex.Empty else regex in
          let string_regex = Regex.regexp_to_string regex in

          Graphics.moveto 300 573;
          Graphics.set_color Graphics.red;
          Graphics.draw_string "Regex reconnue (voir terminal)";
          Printf.printf "La regex reconnue est: %s \n" string_regex;
          flush stdout;
        );

        (*vérifier s'il a appuyé sur la touche 's'*)
        if e.key = 's' then (
          (*Afficher un cercle de rayon "rayon" dont l'origine est à l'emplacement de la souris de l'utilisateur*)
          Graphics.set_color Graphics.black;
          Graphics.draw_circle e.mouse_x e.mouse_y (int_of_float rayon);
          (*Afficher un label sur le cercle*)
          Graphics.moveto e.mouse_x e.mouse_y;
          Graphics.draw_string (string_of_int (Int32.to_int !indice));
          
          (*Ajouter un sommet au graphe*)
          !graphe.sommets <- ref {Graphics_utils.num = !indice; position = {x = Float.of_int e.mouse_x; y = Float.of_int e.mouse_y}; deplacement = {x = 0.; y = 0.}} :: !graphe.sommets;
          
          (*Ajouter un état au NFA*)
          nfa := Nfa.add_state !nfa !indice;

          (*Incrémenter l'indice*)
          indice := Int32.add !indice (Int32.of_int 1);
        );
        (*vérifier s'il a appuyé sur la touche 'e'*)
        if e.key = 't' then (
          (*Vérifier si la souris est sur un sommet (il faut prendre en compte l'origine du sommet et son rayon). Et si oui, extraire le sommet.*)
          let sommet = 
            List.find_opt 
              (fun sommet -> 
                Float.sqrt (((Float.of_int e.mouse_x) -. !sommet.Graphics_utils.position.x) *. ((Float.of_int e.mouse_x) -. !sommet.Graphics_utils.position.x) +. (Float.sub (Float.of_int e.mouse_y) !sommet.Graphics_utils.position.y) *. ((Float.of_int e.mouse_y) -. !sommet.Graphics_utils.position.y)) < rayon) !graphe.Graphics_utils.sommets in
          (*afficher le contenu de sommet*)
          match sommet with
          | None -> ()
          | Some sommet -> (
            if not !bool_sommet_selectionne then (
              bool_sommet_selectionne := not !bool_sommet_selectionne;
              (*Modifier la couleur du sommet en jaune*)
              Graphics.set_color Graphics.yellow;
              Graphics.fill_circle (int_of_float !sommet.position.x) (int_of_float !sommet.position.y) (int_of_float rayon);

              (*On marque le sommet si c'est un état final*)
              if Fsm.StateSet.mem !sommet.num !nfa.Nfa.finals then (
                Graphics.set_color Graphics.black;
                Graphics.draw_circle (int_of_float !sommet.position.x) (int_of_float !sommet.position.y) (int_of_float (rayon /. 1.2));
              );

              (*Afficher un label sur le sommet. Si c'est un état de départ, on affiche "Depart". Sinon, on affiche son numéro.*)
              if Fsm.StateSet.mem !sommet.num !nfa.Nfa.start then (
                Graphics.set_color Graphics.black;
                Graphics.moveto (int_of_float (!sommet.Graphics_utils.position.x -. 17.)) (int_of_float (!sommet.Graphics_utils.position.y -. 5.));
                Graphics.draw_string "Depart";
              ) else (
                Graphics.set_color Graphics.black;
                Graphics.moveto (int_of_float !sommet.Graphics_utils.position.x) (int_of_float !sommet.position.y);
                Graphics.draw_string (string_of_int (Int32.to_int !sommet.num));
              );

            ) else (
              bool_sommet_selectionne := not !bool_sommet_selectionne;

              (*Modifier la couleur du fond du sommet en blanc, car elle a été mise en jaune avant*)
              Graphics.set_color Graphics.white;
              Graphics.fill_circle (int_of_float !sommet_selectionne.Graphics_utils.position.x) (int_of_float !sommet_selectionne.position.y) (int_of_float rayon);

              (*Modifier la couleur du contour du sommet en noir*)
              Graphics.set_color Graphics.black;
              Graphics.draw_circle (int_of_float !sommet_selectionne.Graphics_utils.position.x) (int_of_float !sommet_selectionne.position.y) (int_of_float rayon);

              (*Si le sommet est un état final, on le marque*)
              if Fsm.StateSet.mem !sommet_selectionne.num !nfa.Nfa.finals then (
                Graphics.set_color Graphics.black;
                Graphics.draw_circle (int_of_float !sommet_selectionne.position.x) (int_of_float !sommet_selectionne.position.y) (int_of_float (rayon /. 1.2));
              );

              (*Afficher un label sur le sommet. Si c'est un état un départ, on affiche "Depart", sinon on affiche son numéro*)
              if Fsm.StateSet.mem !sommet_selectionne.num !nfa.Nfa.start then (
                Graphics.set_color Graphics.black;
                Graphics.moveto (int_of_float (!sommet_selectionne.Graphics_utils.position.x -. 17.)) (int_of_float (!sommet_selectionne.Graphics_utils.position.y -. 5.));
                Graphics.draw_string "Depart";
              ) else (
                Graphics.set_color Graphics.black;
                Graphics.moveto (int_of_float !sommet_selectionne.Graphics_utils.position.x) (int_of_float !sommet_selectionne.position.y);
                Graphics.draw_string (string_of_int (Int32.to_int !sommet_selectionne.num));
              );
              clear_information();

              Graphics.moveto 140 573;

              Graphics.set_color Graphics.red;
              Graphics.draw_string "/!\\ Appuyez sur une touche pour ajouter une transition (0 = une epsilon-transition) /!\\";

              (*Attendre l'étiquette associé au sommet*)
              let entree = Graphics.wait_next_event [Graphics.Key_pressed] in

              clear_information();
              Graphics.moveto 205 573;

              Graphics.set_color Graphics.red;
              Graphics.draw_string "Faites un clic souris puis appuyez sur p pour convertir l'automate en regex";


              (*Attendre l'étiquette associé à l'arête*)

              (*Ajouter une arête entre les deux sommets*)
              let copie = !sommet_selectionne in
              !graphe.aretes <- ref {Graphics_utils.origine = ref copie; destination = sommet; label = entree.key} :: !graphe.aretes;

              (*Ajouter un caractère à l'alphabet du NFA*)
              if entree.key <> '0' then  (*On représente le caractère 0 comme epsilon*)
                nfa := Nfa.add_char !nfa entree.key;

              (*Ajouter une transition au NFA*)
              if entree.key = '0' then ( (*On représente une epsilon-transition par le caractère 0*)
                nfa := Nfa.add_transition !nfa !sommet_selectionne.num !sommet.num '\000';
              )
              else (
                nfa := Nfa.add_transition !nfa !sommet_selectionne.num !sommet.num entree.key;
              );

              (*Récupérer la position de départ et d'arrivée de l'arête*)
              let upos = ref!sommet_selectionne.position in
              let vpos = ref !sommet.position in
              let delta = {Graphics_utils.x = Float.sub !vpos.x !upos.x; y = Float.sub !vpos.y !upos.y} in
              let delta_norme = Float.sqrt (Float.mul delta.x delta.x +. Float.mul delta.y delta.y) in
              let delta_sur_norme = {Graphics_utils.x = Float.div delta.x delta_norme; y = Float.div delta.y delta_norme} in
              let delta_sur_norme_x_rayon = {Graphics_utils.x = Float.mul delta_sur_norme.x rayon; y = Float.mul delta_sur_norme.y rayon} in
              let upos = {Graphics_utils.x = Float.add !upos.x delta_sur_norme_x_rayon.x; y = Float.add !upos.y delta_sur_norme_x_rayon.y} in
              let vpos = {Graphics_utils.x = Float.sub !vpos.x delta_sur_norme_x_rayon.x; y = Float.sub !vpos.y delta_sur_norme_x_rayon.y} in
              Graphics.moveto (int_of_float upos.x) (int_of_float upos.y);

              (*afficher l'arrête*)
              Graphics.set_color Graphics.black;
              Graphics_utils.draw_arrow upos vpos;
              
              (*Afficher un label sur l'arête*)
              let label_pos = {Graphics_utils.x = (Float.add !sommet_selectionne.position.x !sommet.position.x) /. 2.; y = (Float.add !sommet_selectionne.position.y !sommet.position.y) /. 2.} in
              (*ajouter un carré blanc à l'endroit du label*)
              Graphics.set_color Graphics.white;
              Graphics.fill_rect (int_of_float label_pos.x) (int_of_float label_pos.y) 10 10;
              (*afficher le label*)
              Graphics.set_color Graphics.black;
              Graphics.moveto (int_of_float label_pos.x) (int_of_float label_pos.y);
              if entree.key = '0' then
                Graphics.draw_string "eps"
              else
                Graphics.draw_string (String.make 1 entree.key);
            );
            sommet_selectionne := !sommet;
          );
        );
        if e.key = 'd' then (
          (*Vérifier si la souris est sur un sommet (il faut prendre en compte l'origine du sommet et son rayon). Et si oui, extraire le sommet.*)
          let sommet = 
            List.find_opt 
              (fun sommet -> 
                Float.sqrt (((Float.of_int e.mouse_x) -. !sommet.Graphics_utils.position.x) *. ((Float.of_int e.mouse_x) -. !sommet.Graphics_utils.position.x) +. (Float.sub (Float.of_int e.mouse_y) !sommet.Graphics_utils.position.y) *. ((Float.of_int e.mouse_y) -. !sommet.Graphics_utils.position.y)) < rayon) !graphe.Graphics_utils.sommets in
          (*afficher le contenu de sommet*)
          match sommet with
          | None -> ()
          | Some sommet -> (
              (*Modifier la couleur du fond du sommet en blanc*)
              Graphics.set_color Graphics.white;
              Graphics.fill_circle (int_of_float !sommet.Graphics_utils.position.x) (int_of_float !sommet.position.y) (int_of_float rayon);

              (*Modifier la couleur du contour du sommet en noir*)
              Graphics.set_color Graphics.black;
              Graphics.draw_circle (int_of_float !sommet.Graphics_utils.position.x) (int_of_float !sommet.position.y) (int_of_float rayon);

              (*Afficher un label sur le sommet*)
              Graphics.set_color Graphics.black;
              Graphics.moveto (int_of_float (!sommet.Graphics_utils.position.x -. 17.)) (int_of_float (!sommet.Graphics_utils.position.y -. 5.));

              (*On marque le sommet si c'est un état final*)
              if Fsm.StateSet.mem !sommet.num !nfa.Nfa.finals then (
                Graphics.set_color Graphics.black;
                Graphics.draw_circle (int_of_float !sommet.position.x) (int_of_float !sommet.position.y) (int_of_float (rayon /. 1.2));
              );

              Graphics.draw_string "Depart";

              (*Ajouter l'état de départ dans le NFA*)
              nfa := Nfa.add_start_state !nfa !sommet.num;
          );
        );
        if e.key = 'x' then (
          (*supprimer un sommet*)
          (*Vérifier si la souris est sur un sommet (il faut prendre en compte l'origine du sommet et son rayon). Et si oui, extraire le sommet.*)
          let sommet = 
            List.find_opt 
              (fun sommet -> 
                Float.sqrt (((Float.of_int e.mouse_x) -. !sommet.Graphics_utils.position.x) *. ((Float.of_int e.mouse_x) -. !sommet.Graphics_utils.position.x) +. (Float.sub (Float.of_int e.mouse_y) !sommet.Graphics_utils.position.y) *. ((Float.of_int e.mouse_y) -. !sommet.Graphics_utils.position.y)) < rayon) !graphe.Graphics_utils.sommets in
          match sommet with
          | None -> ()
          | Some sommet -> (
              (*Clear le graphe*)
              Graphics.clear_graph ();
              
              (*Supprimer le sommet*)
              graphe := Graphics_utils.supprimer_sommet !graphe sommet;
              (*Supprimer les aretes qui partent ou arrivent au sommet*)
              graphe := Graphics_utils.supprimer_aretes !graphe sommet;
              (*Supprimer l'état du NFA*)
              nfa := Nfa.remove_state !nfa !sommet.num;
                
              (*Afficher le graphe*)
              Graphics_utils.dessiner_graphe !graphe !nfa rayon;
              
          );
        );
        if e.key = 'f' then (
          (*Vérifier si la souris est sur un sommet (il faut prendre en compte l'origine du sommet et son rayon). Et si oui, extraire le sommet.*)
          let sommet = 
            List.find_opt 
              (fun sommet -> 
                Float.sqrt (((Float.of_int e.mouse_x) -. !sommet.Graphics_utils.position.x) *. ((Float.of_int e.mouse_x) -. !sommet.Graphics_utils.position.x) +. (Float.sub (Float.of_int e.mouse_y) !sommet.Graphics_utils.position.y) *. ((Float.of_int e.mouse_y) -. !sommet.Graphics_utils.position.y)) < rayon) !graphe.Graphics_utils.sommets in
          (*afficher le contenu de sommet*)
          match sommet with
          | None -> ()
          | Some sommet -> (
              (*Modifier la couleur du fond du sommet en blanc*)
              Graphics.set_color Graphics.white;
              Graphics.fill_circle (int_of_float !sommet.Graphics_utils.position.x) (int_of_float !sommet.position.y) (int_of_float rayon);

              (*Modifier la couleur du contour du sommet en noir*)
              Graphics.set_color Graphics.black;
              Graphics.draw_circle (int_of_float !sommet.Graphics_utils.position.x) (int_of_float !sommet.position.y) (int_of_float rayon);

              (*Marquer le sommet*)
              Graphics.draw_circle (int_of_float !sommet.position.x) (int_of_float !sommet.position.y) (int_of_float (rayon /. 1.2));
                
              (*Afficher un label sur le sommet*)
              if Fsm.StateSet.mem !sommet.num !nfa.Nfa.start then (
                Graphics.set_color Graphics.black;
                Graphics.moveto (int_of_float (!sommet.Graphics_utils.position.x -. 17.)) (int_of_float (!sommet.Graphics_utils.position.y -. 5.));
                Graphics.draw_string "Depart";
              ) else (
                Graphics.set_color Graphics.black;
                Graphics.moveto (int_of_float !sommet.Graphics_utils.position.x) (int_of_float !sommet.position.y);
                Graphics.draw_string (string_of_int (Int32.to_int !sommet.num));
              );
              
              (*Ajouter l'état final dans le NFA*)
              nfa := Nfa.add_final_state !nfa !sommet.num;
        );      
      );
    );
    loop !indice !graphe !bool_sommet_selectionne !sommet_selectionne !nfa;
  );
  in
  let () =
    Graphics.open_graph " 800x600";
    Graphics.set_window_title "Dessiner un NFA";
    let graphe = {Graphics_utils.sommets = []; aretes = []} in
    let nfa = { Nfa.start = Fsm.StateSet.empty;
        finals = Fsm.StateSet.empty;
        all_states = Fsm.StateSet.empty;
        delta = (fun _ -> Fsm.CharMap.empty); 
        alphabet = []
        } in
    (loop 0l graphe false {num = 0l; position = {x = 0.; y = 0.}; deplacement = {x = 0.; y = 0.}} nfa);
    Graphics.close_graph();
  in
  ()
;;

