(*types pour représenter le graphe (représentant l'automate)*)

type coords = {
    mutable x : float;
    mutable y : float;
}

type sommet = {
    num : Fsm.state;
    mutable position : coords;
    mutable deplacement : coords;
}

type arete = {
    label : char;
    mutable origine : sommet ref;
    mutable destination : sommet ref;
}

type graphe = {
  mutable sommets : (sommet ref) list;
  mutable aretes : (arete ref) list;
}

val placement_etats : Nfa.nfa -> int -> int -> int -> graphe 
(** EXPERIMENTAL : [placement_etats nfa] permet de placer les états d'un automate [nfa] de façon circulaire. Cette fonction présente vite ses limites sur certains automates (les états et les transitions se chevauchent).  D'autres heuristiques auraient pu être implémenté, comme l'algorithme de Fruchterman et Reingold (algorithme présenté dans l'article Graph Drawing by Force–Directed Placement en 1991, ref: https://reingold.co/force-directed.pdf) *)

val draw_arrow : coords -> coords -> unit
(** [draw_arrow (x1,y1) (x2,y2)] permet de dessiner une flèche entre les points de coordonnées [(x1,y1)] et [(x2,y2)] *)


val supprimer_sommet : graphe -> sommet ref -> graphe
(** [supprimer_sommet graphe sommet] permet de supprimer le sommet [sommet] du graphe [graphe] *)

val supprimer_aretes : graphe -> sommet ref -> graphe
(** [supprimer_aretes graphe sommet] permet de supprimer toutes les aretes qui partent ou arrivent au sommet [sommet] du graphe [graphe] *)

val dessiner_graphe : graphe -> Nfa.nfa -> float -> unit
(** [dessiner_graphe graphe nfa] permet de dessiner le graphe [graphe] représentant l'automate [nfa] *)