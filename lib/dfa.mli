(* Type pour représenter un automate fini déterministe *)
type dfa = {
  start : Fsm.StateSet.t;
  (** l'état de départ de l'automate *)

  finals: Fsm.StateSet.t list;
  (** les états finaux (ou "acceptants") de l'automate *)

  all_states: Fsm.StateSet.t list;
  (** l'ensemble de tous les états de l'automate *)

  alphabet: Fsm.alphabet;
  (** l'alphabet de l'automate *)

  delta: Fsm.StateSet.t -> Fsm.transitions
  (** la fonction de transition, qui associe un état et un caractère à un état *)
}


val nfa_to_dfa : Nfa.nfa -> dfa
(** Convertit un automate fini non-déterministe en un automate fini déterministe complet  
*)

val print_dfa: dfa -> unit
(** Affiche un automate fini déterministe *)

(** Convertit un automate de type fini et déterministe en un automate de type fini et non-déterministe *)
val dfa_to_nfa : dfa -> Nfa.nfa

