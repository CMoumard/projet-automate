type partition = Fsm.StateSet.t list

type transitions_dfa_min = (Fsm.StateSet.t list) Fsm.CharMap.t

module IntMap = Map.Make(Int32)

type dfa_min = {
  start : partition;
  (** l'état de départ de l'automate *)

  finals: partition list;
  (** les états finaux (ou "acceptants") de l'automate *)

  all_states: partition list;
  (** l'ensemble de tous les états de l'automate *)

  alphabet: Fsm.alphabet;
  (** l'alphabet de l'automate *)

  delta: partition -> transitions_dfa_min;
  (** la fonction de transition, qui associe un état et un caractère à un état *)
}



(* Fonction de minimisation d'un automate fini déterministe (DFA) basé sur l'algorithme de Moore *)
let minimize_dfa (dfa_ : Dfa.dfa) : dfa_min = 
  (* Récupérer les états, les états finaux et la fonction de transition de l'automate *)
  let all_states = dfa_.all_states in
  let finals = dfa_.finals in
  let delta = dfa_.delta in

  (* Initialiser l'ensemble des partitions avec la partition des états finaux et la partition des non-finaux*)
  let partitions = [dfa_.finals; List.filter (fun x -> not (List.mem x finals)) all_states] in

  (* Fonction qui raffine une partition en deux. Si une partition est déjà raffiné au maximum, elle renvoie ([], partition) . Sinon, elle renvoie [partition1, partition2], avec partition1 et partition2 étant le résultat d'un raffinage de partition, tous les deux non nul*)
  let rec raffine partition ens_partitions alphabet = 
    if List.length alphabet = 0 then (
      ([], partition)
    )
    else
      (* Récupérer le premier caractère de l'alphabet *)
      let c = List.hd alphabet in
      (* On récupère le premier état de la partition*)
      let state = List.hd partition in
      (* Indicateur qui annonce un changement de la partition*)
      let change = ref false in
      (* Partition des états qui projettent sur une autre partition que state (tous les états contenu dans cette partition projetteront vers une partition différente de state) *)
      let diff_partitions = ref [] in

      (* Partition des états qui projettent sur la même partition que state*)
      let same_partitions = ref [state] in

      
      (* On récupère l'état atteint par state *)
      let state_reachable = Fsm.CharMap.find c (delta state) in
      (* On récupère la partition atteinte par state *)
      let partition_reachable = List.find (fun partition -> List.mem state_reachable partition) ens_partitions in
      (* Pour chaque état state' de la partition*)
      List.iter (fun state' -> 
        (* On récupère l'état atteint par state' *)
        let state_reachable' = Fsm.CharMap.find c (dfa_.delta state') in
        (* On récupère la partition atteinte par state'.*)
        let partition_reachable' = List.find (fun partition -> List.mem state_reachable' partition) ens_partitions in
        (* On défini une fonction pour comparer deux partitions. (si elles sont égale, cette fonction renverra true, false sinon) *)
        let compare_partitions (l1 : partition) (l2 : partition) : bool =
          List.for_all (fun set1 -> List.exists (Fsm.StateSet.equal set1) l2) l1
          && List.for_all (fun set2 -> List.exists (Fsm.StateSet.equal set2) l1) l2
        in

        if not (compare_partitions partition_reachable partition_reachable') then (
          (* Si la partition atteinte par state et la partition atteinte par state' sont différentes, on ajoute state' dans une partition différente de state => On ajoute state' dans diff_partitions. On remarque que dans ce cas, la partition initiale a été découpé, d'où le changement de la variable change. *)
          change := true;
          diff_partitions := List.cons state' !diff_partitions;
        ) else (
          (* Si la partition atteinte par state et la partition atteinte par state' sont les mêmes, alors on n'a pas besoin de les diviser. On ajoute donc state' dans same_partitions *)
          same_partitions := List.cons state' !same_partitions;
        );
      ) (List.tl partition);
      if !change then (
        (* On renvoie la partition des états qui projettent sur une autre partition et la partition des états qui projettent sur la même partition *)
        (!diff_partitions, !same_partitions)
      ) else (
        raffine partition partitions (List.tl alphabet)
      );
  in
  (* Boucle jusqu'à ce que l'ensemble de partitions ne change plus *)
  let rec loop partitions : partition list = 
    (* Indicateur pour savoir si l'ensemble de partitions a changé *)
    let changed = ref false in
    (* Nouvel ensemble de partitions *)
    let new_partitions = ref [] in
    (* Pour chaque partition de l'ensemble de partitions *)
    List.iter (fun partition -> 
      (* Raffiner la partition *)
      let (partition1, partition2) = raffine partition partitions dfa_.alphabet in
      (* Si la partition a été raffinée *)
      if (List.length partition1) > 0 && (List.length partition2) > 0 then (
        (* On ajoute les deux partitions raffinées à l'ensemble de partitions *)
        new_partitions := List.cons partition1 !new_partitions;
        new_partitions := List.cons partition2 !new_partitions;
        (* On indique que l'ensemble de partitions a changé *)
        changed := true;
      ) else (
        (* On ajoute la partition à l'ensemble de partitions *)
        new_partitions := List.cons partition !new_partitions;
      );
    ) partitions;
    (* Si l'ensemble de partitions a changé, on relance la boucle *)
    if !changed then (
      loop !new_partitions;
    ) else (
      (* Sinon, on renvoie l'ensemble de partitions *)
      !new_partitions;
    );
  in

  (* Lancement de la boucle => On récupère l'ensemble de partitions final *)
  let new_partitions = loop partitions in

  (* On récupère l'état initial. L'état initial est la partition qui contient l'état initial du dfa d'entré. *)
  let start = List.find (fun partition -> List.mem dfa_.start partition) new_partitions in
  (* On récupère les états finaux. Les états finaux sont les partitions qui contiennent au moins un état final du dfa d'entré. *)
  let finals = List.filter (fun partition -> List.exists (fun y -> List.mem y dfa_.finals) partition) new_partitions in
  
  (* On va ensuite utiliser une fonction qui permet de merge plusieurs transitions *)
  let merge_transitions t1 t2 =
    Fsm.CharMap.merge
      (fun _ t1 t2 -> match t1, t2 with
         | Some s, None -> Some s
         | None, Some s -> Some s
         | Some s1, Some s2 -> Some (Fsm.StateSet.union s1 s2)
         | None, None -> None)
      t1 t2 in
  (* Fonction qui à partir d'un état, et de l'ensemble des partitions, donne la partition auquel appartien cet état*)
  let get_partition s partitions = 
    List.find (fun partition ->  List.exists (fun y -> Fsm.StateSet.equal s y) partition) partitions in
  (* Fonction qui modifie les transitions en fonction des partitions *)
  let rec modify_transitions (transitions : Fsm.transitions) partitions = 
    Fsm.CharMap.mapi (fun c s -> get_partition s partitions) transitions in
  (* On défini la fonction delta fonctionnant ainsi: pour chaque état de la partition, on récupère les transitions de l'état. On merge ces transitions, puis on converti l'ensemble de transitions obtenu. Ainsi, si on avait une transition d'un état q par le caractère c à un état q', on substitue q' avec sa partition*)
  let delta = fun s -> 
    let all_transitions = List.map (fun q -> dfa_.delta q) s in
    (modify_transitions (List.fold_left merge_transitions Fsm.CharMap.empty all_transitions) new_partitions)
  in
  (* On supprime enfin le puit, c'est à dire la partition contenant uniquement StateSet.empty *)
  let all_states = List.filter 
    (fun partition -> 
      not (List.for_all 
        (fun state -> Fsm.StateSet.equal state Fsm.StateSet.empty) partition)
    ) new_partitions
  in
  (* On supprime toutes les transition vers cet état puit. On doit donc itérer dans l'ensemble des transitions delta, et supprimer l'entrée si elle pointe vers cet état puit *)
  let delta' = fun s -> 
    let transitions = delta s in
    Fsm.CharMap.filter (fun c s' -> not (List.exists (fun state -> Fsm.StateSet.equal state Fsm.StateSet.empty) s' )) transitions
  (* On renvoie le dfa_min *)
  in
  {start = start; finals = finals;
  alphabet = dfa_.alphabet;
  all_states = all_states; delta = delta'}
;;


(* Fonction qui affiche un dfa_min dans la sortie standard *)
let print_dfa_min dfa_min = 
  let print_partition partition = 
    List.iter (Fsm.StateSet.iter (fun s -> Printf.printf "%ld " s)) partition;
  in
  Printf.printf "Start state: ";
  print_partition dfa_min.start;
  Printf.printf "\nFinal states: ";
  List.iter (fun partition -> print_partition partition; Printf.printf " ") dfa_min.finals;
  Printf.printf "\nAll states: ";
  List.iter (fun partition -> print_partition partition; Printf.printf " ") dfa_min.all_states;
  Printf.printf "\nTransitions:";
  print_newline() ;
  List.iter (fun partition -> 
    Printf.printf "partition: ";
    print_partition partition;
    Printf.printf " -> ";
    Fsm.CharMap.iter (fun c partition' -> 
      Printf.printf "%c: " c;
      print_partition partition';
      Printf.printf " "
    ) (dfa_min.delta partition);
    Printf.printf ";";
    print_newline ();
  ) dfa_min.all_states;
;;


(** Convertion d'une expression régulière en automate d'état fini déterministe minimal*)
let regexp_to_dfa_min (r : Regex.regexp) : dfa_min = 
  Nfa.thompson r |> Dfa.nfa_to_dfa |> minimize_dfa



(** Conversion  d'un type DFAmin vers un type NFA*)
let dfa_min_to_nfa (dfamin : dfa_min) : Nfa.nfa =
  (*fonction prenant en entrée un ensemble de partition, et renvoyant un dictionnaire qui associe à chaque partition un entier*)
  let rec get_dict partitions dict i = 
    match partitions with
    | [] -> dict
    | partition::partitions' -> 
      get_dict partitions' (IntMap.add i partition dict) (Int32.add i 1l)
  in
  let find_key_by_value map value =
    let key_ref = ref None in
    IntMap.iter (fun k v -> if v = value then key_ref := Some k) map;
    Option.get !key_ref
  in
  let dict = get_dict dfamin.all_states IntMap.empty 0l in
  let start = Fsm.StateSet.of_list [find_key_by_value dict dfamin.start] in
  let finals = Fsm.StateSet.of_list (List.map (fun s -> find_key_by_value dict s) dfamin.finals) in
  let all_states = Fsm.StateSet.of_list (IntMap.bindings dict |> List.map (fun (k, _) -> k)) in
  let delta = fun s -> 
    let partition = IntMap.find s dict in
    Fsm.CharMap.map (fun partition' -> Fsm.StateSet.of_list [find_key_by_value dict partition']) (dfamin.delta partition)
  in
  {start = start; finals = finals; alphabet = dfamin.alphabet; all_states = all_states; delta = delta}
;;
  (*fonction prenant en entrée un ensemble de partition, et renvoyant un entier*)
  



;;